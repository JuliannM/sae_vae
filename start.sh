rm -rf bin

javac -d bin -classpath bin -sourcepath src --module-path /usr/share/openjfx/lib/ --add-modules javafx.controls src/*.java

java -ea -cp .:/usr/share/java/mariadb-java-client.jar:bin --module-path /usr/share/openjfx/lib/ --add-modules javafx.controls Executable