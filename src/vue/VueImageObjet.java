package vue;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.nio.file.Files;

public class VueImageObjet extends ImageView{
    
    private byte[] img;
    private String imageParDefaut;

    public VueImageObjet(String imageParDefaut){
        this.imageParDefaut=imageParDefaut;
        this.setImageObjet(imageParDefaut);
    }

    public  void resetImageObjet(){
        this.setImageObjet(this.imageParDefaut);
    }
    public void setImageObjet(String nomFichier){
        File f= new File(nomFichier);
        try {

            this.img=Files.readAllBytes(f.toPath());
        }catch (Exception ex){this.img=null;}
        this.setImage(new Image(new ByteArrayInputStream(this.img), 125, 125, false, false));
    }

    public void setImageObjet(byte[] img) {
        if (img != null){
            this.img = img;
            this.setImage(new Image(new ByteArrayInputStream(this.img), 125, 125, false, false));
        }
        else
            this.resetImageObjet();
    }
    public byte[] getImageObjet(){ return this.img;}
}
