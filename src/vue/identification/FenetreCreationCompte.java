package vue.identification;

import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.layout.VBox;
import javafx.geometry.Insets;
import javafx.scene.text.Text;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;

import javafx.scene.control.Label;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.control.TextField;
import javafx.scene.control.PasswordField;
import javafx.scene.layout.GridPane;

import controleur.ControleurBoutonIdentification;
import controleur.ControleurInscription;
import controleur.ControleurSouris;
import vue.*;

public class FenetreCreationCompte extends FenetreBaseIdentification{

    private PasswordField pF1;
    private PasswordField pF2;
    private TextField pseudo;
    private TextField email;

    private Label mdp2Label;
    private Label mdpLabel;
    private Label pseudoLabel;
    private Label emailLabel;
    
    public FenetreCreationCompte(ApplicationVAE app, double width, double height){
        super(app, width, height);

        VBox line1 = new VBox();
        line1.setMaxWidth(400);
        line1.setMinWidth(400);
        line1.setMaxHeight(2);
        line1.setMinHeight(2);
        line1.setBackground(new Background(new BackgroundFill(Color.BLACK, null, null)));

        VBox center = new VBox();

        Text titre = new Text("Inscription");
        titre.setFont(Font.font("Arial", FontWeight.BOLD, 30));

        GridPane grid = new GridPane();
        grid.setVgap(20);
        grid.setHgap(5);
        grid.setPadding(new Insets(10, 10, 10, 10));

        this.pF1 = new PasswordField();
        this.pF1.setPromptText("Mot de passe");

        this.pF2 = new PasswordField();
        this.pF2.setPromptText("Mot de passe");

        this.pseudo = new TextField();
        this.pseudo.setPromptText("Pseudo");

        this.email = new TextField();
        this.email.setPromptText("Email");

        Button inscription = new Button("S'inscrire");
        inscription.setId("inscrire");
        inscription.setOnAction(new ControleurInscription(app));

        inscription.setStyle("-fx-background-radius: 20px;-fx-background-color: #A47F29;");
        inscription.setMaxWidth(200);
        inscription.setMinWidth(200);
        inscription.setMaxHeight(50);
        inscription.setMinHeight(50);
        inscription.setFont(Font.font("Arial", FontWeight.BOLD, 15));

        ControleurSouris controleurSouris = new ControleurSouris(inscription);
        inscription.setOnMouseEntered(controleurSouris);
        inscription.setOnMouseExited(controleurSouris);

        Text pseudoText = new Text("Pseudo : ");
        pseudoText.setFont(Font.font("Arial", FontWeight.BOLD, 15));

        Text emailText = new Text("Email : ");
        emailText.setFont(Font.font("Arial", FontWeight.BOLD, 15));

        Text mdpText = new Text("Mot de passe : ");
        mdpText.setFont(Font.font("Arial", FontWeight.BOLD, 15));

        Text mdpText2 = new Text("Confirmer le mot de passe : ");
        mdpText2.setFont(Font.font("Arial", FontWeight.BOLD, 15));

        grid.add(pseudoText, 0, 0);
        grid.add(emailText, 0, 2);
        grid.add(mdpText, 0, 4);
        grid.add(mdpText2, 0, 6);

        this.pseudoLabel = new Label("");
        this.pseudoLabel.setTextFill(Color.RED);

        this.emailLabel = new Label("");
        this.emailLabel.setTextFill(Color.RED);

        this.mdpLabel = new Label("");
        this.mdpLabel.setTextFill(Color.RED);

        this.mdp2Label = new Label("");
        this.mdp2Label.setTextFill(Color.RED);
    
        grid.add(this.pseudo, 1, 0);
        grid.add(this.pseudoLabel, 1, 1);

        grid.add(this.email, 1, 2);
        grid.add(this.emailLabel, 1, 3);

        grid.add(this.pF1, 1, 4);
        grid.add(this.mdpLabel, 1, 5);

        grid.add(this.pF2, 1, 6);
        grid.add(this.mdp2Label, 1, 7);

        grid.add(inscription, 1, 8);
        grid.setAlignment(Pos.CENTER);

        center.getChildren().addAll(titre, line1, grid);
        center.setAlignment(Pos.TOP_CENTER);
        center.setPadding(new Insets(20, 20, 20, 20));
        center.setSpacing(30);

        VBox vboxLeft = new VBox();
        Button retour = new Button("");
        retour.setId("retour");
        retour.setOnAction(new ControleurBoutonIdentification(app));
        retour.setGraphic(new ImageView(new Image("file:img/retour.png", 30, 30, false, false)));
        retour.setStyle("-fx-background-radius: 10px;-fx-background-color: #A47F29;");
        vboxLeft.getChildren().add(retour);
        vboxLeft.setAlignment(Pos.BOTTOM_LEFT);

        controleurSouris = new ControleurSouris(retour);

        retour.setOnMouseEntered(controleurSouris);
        retour.setOnMouseExited(controleurSouris);

        center.getChildren().add(vboxLeft);

        this.setCenter(center);
    }

    public String getMotDePasse(){
        return this.pF1.getText();
    }

    public String getMotDePasse2(){
        return this.pF2.getText();
    }

    public String getPseudo(){
        return this.pseudo.getText();
    }

    public String getEmail(){
        return this.email.getText();
    }

    public void setMdpLabel(String value){
        this.mdpLabel.setText(value);
    }

    public void setMdp2Label(String value){
        this.mdp2Label.setText(value);
    }

    public void setPseudoLabel(String value){
        this.pseudoLabel.setText(value);
    }

    public void setEmailLabel(String value){
        this.emailLabel.setText(value);
    }
}