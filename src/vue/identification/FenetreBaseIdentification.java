package vue.identification;

import java.util.Arrays;
import java.util.List;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;

import vue.*;

// Contruit le fond de la fenetre d'identification
public class FenetreBaseIdentification extends BorderPane{

    public FenetreBaseIdentification(ApplicationVAE app, double width, double height){
        ImageView logo = new ImageView(new Image("file:img/logo.png", 200, 200, false, false));
        

        BorderPane top = new BorderPane();
        top.setMaxWidth(width);
        top.setMinWidth(width);
        top.setMaxHeight(height*0.25);
        top.setMinHeight(height*0.25);

        top.setCenter(logo);

        top.setBackground(new Background(new BackgroundFill(Color.valueOf("1F1E1E"), null, null)));
        
        this.setTop(top);

        VBox bottom = new VBox();
        bottom.setBackground(new Background(new BackgroundFill(Color.valueOf("1F1E1E"), null, null)));

        bottom.setMaxWidth(width);
        bottom.setMinWidth(width);
        bottom.setMaxHeight(height*0.15);
        bottom.setMinHeight(height*0.15);

        VBox logos = new VBox();

        HBox reseaux = new HBox();
        List<String> nomImg = Arrays.asList("Facebook", "In", "Insta", "Twitter", "Yt");
        for(String nom : nomImg){
            ImageView imgReseau = new ImageView(new Image("file:img/reseaux/"+nom+".png", 35, 30, false, true));
            reseaux.getChildren().add(imgReseau);
        }
        HBox langues = new HBox();
        for(int i=0;i<=6;i++){
            ImageView imgLangue = new ImageView(new Image("file:img/drapeau/"+i+".png", 30, 30, false, true));
            langues.getChildren().add(imgLangue);
        }

        logos.getChildren().addAll(reseaux, langues);

        bottom.getChildren().addAll(logos);
        langues.setAlignment(Pos.CENTER);
        langues.setSpacing(10);
        reseaux.setSpacing(10);
        reseaux.setAlignment(Pos.CENTER);
        logos.setPadding(new Insets(10, 10, 10, 10));
        logos.setSpacing(10);

        this.setBottom(bottom);

    }
}
