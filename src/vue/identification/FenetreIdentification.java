package vue.identification;

import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.geometry.Insets;
import javafx.scene.text.Text;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;

import javafx.scene.text.FontWeight;

import controleur.ControleurBoutonIdentification;
import controleur.ControleurSouris;

import vue.*;

public class FenetreIdentification extends FenetreBaseIdentification{
    
    public FenetreIdentification(ApplicationVAE app, double width, double height){
        super(app, width, height);

        VBox line1 = new VBox();
        line1.setMaxWidth(400);
        line1.setMinWidth(400);
        line1.setMaxHeight(2);
        line1.setMinHeight(2);
        line1.setBackground(new Background(new BackgroundFill(Color.BLACK, null, null)));
        
        VBox line2 = new VBox();
        line2.setMaxWidth(200);
        line2.setMinWidth(200);
        line2.setMaxHeight(2);
        line2.setMinHeight(2);
        line2.setBackground(new Background(new BackgroundFill(Color.BLACK, null, null)));

        VBox line3 = new VBox();
        line3.setMaxWidth(200);
        line3.setMinWidth(200);
        line3.setMaxHeight(2);
        line3.setMinHeight(2);
        line3.setBackground(new Background(new BackgroundFill(Color.BLACK, null, null)));


        VBox vBoxCenter = new VBox();
        vBoxCenter.setSpacing(50);

        Text titre1 = new Text("BIENVENUE");
        Text titre2 = new Text("SUR L'APPLI VAE !");
        titre1.setFill(Color.BLACK);
        titre1.setFont(Font.font("Arial", FontWeight.BOLD, 30));
        titre2.setFill(Color.BLACK);
        titre2.setFont(Font.font("Arial", FontWeight.BOLD, 30));

        Text texte = new Text("Notre application de vente aux enchères à l'international !");
        texte.setFill(Color.BLACK);
        texte.setFont(Font.font("Arial", FontWeight.NORMAL, 15));

        VBox vBoxCenter2 = new VBox();
        vBoxCenter2.getChildren().addAll(titre1, titre2);
        vBoxCenter2.setSpacing(5);
        vBoxCenter2.setAlignment(Pos.TOP_CENTER);
        vBoxCenter2.setPadding(new Insets(10, 10, 20, 10));

        Text connexion = new Text("Connecte toi ou crée un compte :");
        connexion.setFont(Font.font("Arial", FontWeight.BOLD, 20));

        Button creation = new Button("Crée un compte");
        creation.setStyle("-fx-background-radius: 20px;-fx-background-color: #A47F29;");
        creation.setMaxWidth(200);
        creation.setMinWidth(200);
        creation.setMaxHeight(50);
        creation.setMinHeight(50);
        creation.setFont(Font.font("Arial", FontWeight.BOLD, 15));
        creation.setId("creation");
        ControleurSouris controleurSouris = new ControleurSouris(creation);
        creation.setOnMouseEntered(controleurSouris);
        creation.setOnMouseExited(controleurSouris);
        
        Text ouText = new Text("OU");
        ouText.setFont(Font.font("Arial", FontWeight.BOLD, 20));

        HBox ou = new HBox();
        ou.setSpacing(5);
        ou.getChildren().addAll(line2, ouText, line3);
        ou.setAlignment(Pos.CENTER);

        Button connexionButton = new Button("Connexion");
        connexionButton.setStyle("-fx-background-radius: 20px;-fx-background-color: #A47F29;");
        connexionButton.setMaxWidth(200);
        connexionButton.setMinWidth(200);
        connexionButton.setMaxHeight(50);
        connexionButton.setMinHeight(50);
        connexionButton.setFont(Font.font("Arial", FontWeight.BOLD, 15));
        connexionButton.setId("connexion");

        controleurSouris = new ControleurSouris(connexionButton);
        connexionButton.setOnMouseEntered(controleurSouris);
        connexionButton.setOnMouseExited(controleurSouris);

        VBox vboxCenter3 = new VBox();
        vboxCenter3.getChildren().addAll(connexion, creation);
        vboxCenter3.setAlignment(Pos.CENTER);
        vboxCenter3.setSpacing(20);

        vBoxCenter.getChildren().addAll(vBoxCenter2, texte, line1, vboxCenter3, ou, connexionButton);

        vBoxCenter.setAlignment(Pos.TOP_CENTER);

        creation.setOnAction(new ControleurBoutonIdentification(app));
        connexionButton.setOnAction(new ControleurBoutonIdentification(app));

        this.setCenter(vBoxCenter);
    }
}
