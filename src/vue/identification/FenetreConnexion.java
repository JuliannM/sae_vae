package vue.identification;

import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javafx.geometry.Insets;
import javafx.scene.text.Text;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;

import javafx.scene.text.FontWeight;

import controleur.ControleurBoutonConnexionVAE;
import controleur.ControleurBoutonIdentification;
import controleur.ControleurSouris;
import vue.*;

public class FenetreConnexion extends FenetreBaseIdentification{
    
    private TextField pseudo;
    private PasswordField mdp;

    public FenetreConnexion(ApplicationVAE app, double width, double height){
        super(app, width, height);

        VBox line1 = new VBox();
        line1.setMaxWidth(400);
        line1.setMinWidth(400);
        line1.setMaxHeight(2);
        line1.setMinHeight(2);
        line1.setBackground(new Background(new BackgroundFill(Color.BLACK, null, null)));

        VBox vBoxCenter = new VBox();
        vBoxCenter.setSpacing(50);

        Text titre1 = new Text("Connexion");
        titre1.setFill(Color.BLACK);
        titre1.setFont(Font.font("Arial", FontWeight.BOLD, 30));

        Text texte = new Text("Connecte toi avec ton pseudo et ton mot de passe :");
        texte.setFill(Color.BLACK);
        texte.setFont(Font.font("Arial", FontWeight.NORMAL, 15));

        VBox vBoxCenter2 = new VBox();
        vBoxCenter2.getChildren().addAll(titre1);
        vBoxCenter2.setSpacing(5);
        vBoxCenter2.setAlignment(Pos.TOP_CENTER);
        vBoxCenter2.setPadding(new Insets(20, 20, 20, 20));
    

        Button connexionButton = new Button("Connexion");
        connexionButton.setId("connexion2");
        connexionButton.setOnAction(new ControleurBoutonConnexionVAE(app));
        connexionButton.setStyle("-fx-background-radius: 20px;-fx-background-color: #A47F29;");
        connexionButton.setMaxWidth(200);
        connexionButton.setMinWidth(200);
        connexionButton.setMaxHeight(50);
        connexionButton.setMinHeight(50);
        connexionButton.setFont(Font.font("Arial", FontWeight.BOLD, 15));
        ControleurSouris ControleurSourisconnexion2 = new ControleurSouris(connexionButton);
        connexionButton.setOnMouseEntered(ControleurSourisconnexion2);
        connexionButton.setOnMouseExited(ControleurSourisconnexion2);

        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(20);
        grid.setAlignment(Pos.CENTER);

        Label labelps = new Label("Pseudo :");
        this.pseudo = new TextField();
        this.pseudo.setPromptText("Pseudo");
        Label labelmdp = new Label("Mot de passe :");
        this.mdp = new PasswordField();
        this.mdp.setPromptText("Mot de passe");

        grid.add(labelps, 0, 0);
        grid.add(this.pseudo, 1, 0);
        grid.add(labelmdp, 0, 1);
        grid.add(this.mdp, 1, 1);

        labelps.setFont(Font.font("Arial", FontWeight.BOLD, 15));
        labelmdp.setFont(Font.font("Arial", FontWeight.BOLD, 15));
        VBox vBoxLeft = new VBox();
        Button retourButton = new Button("");
        retourButton.setId("retour");
        retourButton.setOnAction(new ControleurBoutonIdentification(app));
        retourButton.setGraphic(new ImageView(new Image("file:img/retour.png",30,30,false,false)));
        retourButton.setStyle("-fx-background-radius: 10px;-fx-background-color: #A47F29;");
        ControleurSouris ControleurSourisretour = new ControleurSouris(retourButton);
        retourButton.setOnMouseEntered(ControleurSourisretour);
        retourButton.setOnMouseExited(ControleurSourisretour);
        vBoxLeft.getChildren().addAll(retourButton);
        vBoxLeft.setAlignment(Pos.BOTTOM_LEFT);


        vBoxCenter.getChildren().addAll(vBoxCenter2, texte,line1,grid, connexionButton, vBoxLeft);
        vBoxCenter.setPadding(new Insets(20, 20, 20, 20));
        vBoxCenter.setAlignment(Pos.CENTER);
        
        this.setCenter(vBoxCenter);
    }
        
        
    public String getPseudo(){
        return this.pseudo.getText();
    }

    public String getMdp(){
        return this.mdp.getText();
    }
}