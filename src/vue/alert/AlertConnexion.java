package vue.alert;

import javafx.scene.control.Alert;
public class AlertConnexion extends Alert{

    public AlertConnexion(){
        super(Alert.AlertType.ERROR);
        this.setTitle("Erreur");
        this.setHeaderText("Erreur lors de la connexion");
        this.setContentText("Mot de passe ou pseudo incorrect");
        this.showAndWait();
    }


    
}
