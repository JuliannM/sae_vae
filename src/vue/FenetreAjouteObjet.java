package vue;

import javafx.scene.control.DatePicker;
import javafx.scene.control.TextArea;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.layout.HBox;
import javafx.geometry.Insets;
import javafx.scene.text.*;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.scene.control.ScrollPane;
import javafx.stage.FileChooser;
import java.io.File;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import java.util.List;
import java.io.File;
import java.util.Arrays;

import vue.accueil.FenetreBaseAccueil;

import vue.ApplicationVAE;
import vue.perso.FenetreBaseEspacePerso;

public class FenetreAjouteObjet extends BorderPane{

    private double width;
    private double height;

    private int ligne;
    private int col;

    private ImageView imageProfil;

    private FileChooser choixFichier;
    private Button changeImage;


    private TextField nomTextField;
    private TextArea descriptionTextArea;
    private TextField prixMinTextField;
    private TextField prixBaseTextField;
    private DatePicker debutVentePicker;
    private DatePicker finVentePicker;

    public FenetreAjouteObjet(ApplicationVAE app, double width, double height){
        super();
        this.width = width;
        this.height = height;
        
        this.choixFichier = new FileChooser();   
        this.changeImage = new Button("Changer l'image");
        this.changeImage.setPrefWidth(180);
        this.changeImage.setStyle("-fx-background-radius: 20px;-fx-background-color: #A47F29;");
        this.changeImage.setFont(Font.font("Arial", FontWeight.BOLD, 14));
        
        Text entrez = new Text("Entrez les informations du produit :");
        entrez.setFont(Font.font("Arial", FontWeight.BOLD, 21));

        GridPane grille = new GridPane();

        Text nom = new Text("Nom :");
        nom.setFont(Font.font("Arial", 18));

        Text description = new Text("Description :");
        description.setFont(Font.font("Arial", 18));
        
        Text prixDeBase = new Text("Prix de base :");
        prixDeBase.setFont(Font.font("Arial", 18));

        Text prixMin = new Text("Prix minimum :");
        prixMin.setFont(Font.font("Arial", 18));

        Text debutVente = new Text("Début de la vente :");
        debutVente.setFont(Font.font("Arial", 18));

        Text finVente = new Text("Fin de la vente :");
        finVente.setFont(Font.font("Arial", 18));

        this.nomTextField = new TextField();
        this.nomTextField.setPromptText("Nom du produit");
        this.descriptionTextArea = new TextArea();
        this.descriptionTextArea.setPromptText("Description du produit");
        this.descriptionTextArea.setPrefSize(300, 60);
        this.prixMinTextField = new TextField();
        this.prixMinTextField.setPromptText("en €");
        this.prixBaseTextField = new TextField();
        this.prixBaseTextField.setPromptText("en €");

        this.debutVentePicker = new DatePicker();
        this.finVentePicker = new DatePicker();

        VBox vboxImage = getImage();
        vboxImage.setPadding(new Insets(50, 100, 0, 0));
        HBox rassemblement = new HBox();

        grille.add(nom, 0, 0);
        grille.add(description, 0, 2);
        grille.add(prixDeBase, 0, 4);
        grille.add(prixMin, 0, 6);
        grille.add(debutVente, 0, 8);
        grille.add(finVente, 0, 10);

        grille.add(nomTextField, 1, 0);
        grille.add(descriptionTextArea, 1, 2);
        grille.add(prixBaseTextField, 1, 4);
        grille.add(prixMinTextField, 1, 6);
        grille.add(debutVentePicker, 1, 8);
        grille.add(finVentePicker, 1, 10);

        grille.setAlignment(Pos.CENTER);

        grille.setPadding(new Insets(11));
        grille.setHgap(30);
        grille.setVgap(8);

        rassemblement.getChildren().addAll(vboxImage, grille);

        rassemblement.setAlignment(Pos.CENTER);

        Button mettreEnVente = new Button("Mettre en vente");
        mettreEnVente.setPrefWidth(300);
        mettreEnVente.setStyle("-fx-background-radius: 20px;-fx-background-color: #A47F29;");
        mettreEnVente.setFont(Font.font("Arial", FontWeight.BOLD, 20));

        VBox nouveauProduit = new VBox();

        nouveauProduit.getChildren().addAll(entrez, rassemblement, mettreEnVente);
        nouveauProduit.setPadding(new Insets(18));
        nouveauProduit.setSpacing(40);
        nouveauProduit.setAlignment(Pos.CENTER);

        this.setCenter(nouveauProduit);


        // VBOX BOTTOM

        VBox vboxTOP = new VBox();
        Background couleur = new Background(new BackgroundFill(Color.valueOf("#1F1E1E"), null, null));
        vboxTOP.setBackground(couleur);
        this.setTop(vboxTOP);

        // Haut de la VBox
        BorderPane borderPaneTOP = new BorderPane();


        StackPane logo = new StackPane();
        logo.setPadding(new Insets(20));
        ImageView imgLogo = new ImageView(new Image("file:img/logo.jpeg", 160, 160, false, false));
        logo.getChildren().add(imgLogo);
        borderPaneTOP.setCenter(logo);


        //barre de recherche 


        HBox boxText = new HBox();
        Text message = new Text("Ajouter un objet");
        message.setFont(Font.font("Arial", FontWeight.BOLD, 25));
        message.setFill(Color.WHITE);
        boxText.getChildren().add(message);
        boxText.setAlignment(Pos.CENTER);
        boxText.setPadding(new Insets(20));


        //Cagntotte 

        Button boutonCagnotte = new Button();
        boutonCagnotte.setBackground(couleur);
        boutonCagnotte.setGraphic(new ImageView(new Image("file:img/cagnotte.png", 50, 50, false, false)));
        borderPaneTOP.setRight(boutonCagnotte);


        vboxTOP.getChildren().addAll(borderPaneTOP, boxText);

        VBox vboxBOT = new VBox();
        vboxBOT.setBackground(couleur);
        vboxBOT.setSpacing(10);
        vboxBOT.setAlignment(Pos.TOP_CENTER);
        vboxBOT.setMaxHeight(100);
        vboxBOT.setMinHeight(100);
        


        Label messageReseau = new Label("Nos Réseaux :");
        messageReseau.setFont(Font.font("Arial", FontWeight.NORMAL, 12));
        messageReseau.setTextFill(Color.WHITE);
        messageReseau.setPadding(new Insets(10, 0, 0, 0));

        HBox reseaux = new HBox();
        List<String> nomImg = Arrays.asList("Facebook", "In", "Insta", "Twitter", "Yt");
        for (String nomImage : nomImg) {
            ImageView imgReseau = new ImageView(new Image("file:img/reseaux/" + nomImage + ".png", 35, 30, false, true));
            reseaux.getChildren().add(imgReseau);
        }

        reseaux.setPadding(new Insets(0, 10, 10, 10));
        reseaux.setSpacing(10);
        reseaux.setAlignment(Pos.CENTER);
        vboxBOT.getChildren().addAll(messageReseau, reseaux);

        this.setBottom(vboxBOT);


        // SCROLL

        ScrollPane scrollPane = new ScrollPane(nouveauProduit);
        scrollPane.setFitToWidth(true);
        this.setCenter(scrollPane);
    }

    private VBox getImage() {
        VBox vboxBouton = new VBox();
        vboxBouton.setAlignment(Pos.CENTER);
    
        choixFichier.getExtensionFilters().add(new FileChooser.ExtensionFilter("Images", "*.png", "*.jpg", "*.jpeg"));
        File file = new File("imgProfil/fast_And_Furious.png");
        System.out.println(file.toURI().toString());
        imageProfil = new ImageView(new Image(file.toURI().toString()));
        imageProfil.setFitHeight(250);
        imageProfil.setFitWidth(250);
        changeImage.setOnAction(e -> {
            File imageChoisi = choixFichier.showOpenDialog(null);
    
            if (imageChoisi != null) {
                Image lien = new Image(imageChoisi.toURI().toString());
                imageProfil.setImage(lien);
            }
        });

        HBox hboxImage = new HBox();
        hboxImage.getChildren().add(this.changeImage);
        hboxImage.setAlignment(Pos.CENTER);
        vboxBouton.getChildren().addAll(imageProfil, hboxImage);
        vboxBouton.setSpacing(40);
        vboxBouton.setAlignment(Pos.TOP_LEFT);
        return vboxBouton;
    }   
}