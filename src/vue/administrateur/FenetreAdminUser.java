package vue.administrateur;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.text.Font;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.geometry.Pos;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.scene.control.ButtonBar.ButtonData;
import java.util.List;
import javax.swing.GroupLayout;
import javax.swing.border.EmptyBorder;

import java.util.Arrays;
import java.io.File;
import java.util.ArrayList;
import javafx.util.Duration;
import javafx.scene.shape.Rectangle;
import javafx.scene.Node;
import javafx.util.Duration;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.animation.TranslateTransition;
import javafx.geometry.Pos;
import javafx.scene.text.FontWeight;
import vue.perso.FenetreBaseEspacePerso;

import java.util.ArrayList;
import java.util.List;

import modele.*;
import vue.*;

public class FenetreAdminUser extends FenetreAdminBase {

    private double width;
    private double height;
    private ApplicationVAE vae;

    public FenetreAdminUser(ApplicationVAE vae, double width, double height) {
        super(vae, width, height);
        this.width = width;



        // VBOX CENTER

        VBox vboxAll = new VBox();

        VBox vboxUtilisateur = new VBox();

        // Création des HBox des messages
        //     public Utilisateur(double id, String pseudonyme, String mail, String tel, String mdp, int cagnotte){
        for (int i = 2; i < 14; i += 2) {
            Utilisateur utilisateur1 = new Utilisateur(i - 1, "Test", "test1@gamil.com", "test1", 0);
            Utilisateur utilisateur2 = new Utilisateur(i, "Test", "test2@gamil.com", "test2", 0);
            
            HBox hbox = new HBox();
            hbox.getChildren().addAll(utilisateurVu(utilisateur1), utilisateurVu(utilisateur2));
            hbox.setAlignment(Pos.CENTER);
            vboxUtilisateur.getChildren().add(hbox);
        }
            

        //creation de la VBox pour la barre de recherche 

        BorderPane borderSearch = new BorderPane();
        borderSearch.setPadding((new Insets(30,0,50,0)));
        borderSearch.setBackground(new Background(new BackgroundFill(Color.WHITESMOKE, null, null)));
        
        HBox hboxSearchbar = new HBox();
        hboxSearchbar.setMaxWidth(550);
        // hboxSearchbar.setBackground(new Background(new BackgroundFill(Color.RED, null, null)));
        hboxSearchbar.setStyle("-fx-border-radius: 20; -fx-border-color: #B5B2B3; -fx-border-width: 2; -fx-padding: 5; -fx-background-color: #F5F5F5; -fx-background-insets: 7;");
        hboxSearchbar.setAlignment(Pos.CENTER);//mettre au centre
        //permet de gérer les bords arrondis

        //creation du textField 

        TextField searchbar = new TextField();
        searchbar.setPromptText("Entrez un nom d'utilisateur ... ");
        searchbar.setBackground(new Background(new BackgroundFill(Color.WHITESMOKE, null, null)));
        searchbar.setPrefWidth(500);

        hboxSearchbar.getChildren().add(searchbar);

        //button recherche 
        Button research = new Button();
        research.setBackground(new Background(new BackgroundFill(Color.WHITESMOKE, null, null)));
        research.setGraphic(new ImageView(new Image("file:img/loupe.png", 18, 18, false, false)));
        hboxSearchbar.getChildren().add(research);
        
        VBox vboxtop = new VBox();
        vboxtop.setAlignment(Pos.CENTER);
        HBox ligne = getRectangle();
        ligne.setPadding(new Insets(40, 0, 0, 0));
        vboxtop.getChildren().addAll(hboxSearchbar, ligne);
        borderSearch.setCenter(vboxtop);


// SCROLLPANE

        ScrollPane scrollPane = new ScrollPane(vboxUtilisateur);
        scrollPane.setFitToWidth(true);
        scrollPane.setFitToHeight(true);
        scrollPane.setPadding(new Insets(0));
        vboxAll.getChildren().addAll(borderSearch, scrollPane);
        this.setCenter(vboxAll);
    }



    // Méthode qui retourne la HBox qui gère les informations de l'utilisateur

    public HBox utilisateurVu(Utilisateur utilisateur){
        HBox boxUtilisateur = new HBox();

        BorderPane infoPane = new BorderPane();
        BorderPane top = new BorderPane();

        Label label = new Label("Identifiant : "+utilisateur.getID());
        label.setPadding(new Insets(5));

        Button buttonSupp = new Button();
        ImageView sup = new ImageView(new Image("file:img/supprimer.png"));
        sup.setFitHeight(20);
        sup.setFitWidth(15);
        buttonSupp.setGraphic(sup);

        buttonSupp.setStyle("-fx-background-radius: 20px;-fx-background-color: #FFFFFF;");
        buttonSupp.setOnMouseEntered(event -> {
            buttonSupp.setStyle("-fx-background-radius: 20px;-fx-background-color: #A47f29;");
        });
        buttonSupp.setOnMouseExited(event -> {
            buttonSupp.setStyle("-fx-background-radius: 20px;-fx-background-color: #FFFFFF;");
        });

        top.setLeft(label);
        top.setRight(buttonSupp);

        infoPane.setTop(top);
        infoPane.getTop().setStyle("-fx-padding: 5 30 5 5;");

        VBox info = new VBox();
        Label nom = new Label("Nom : "+utilisateur.getPseudonyme());
        Label mail = new Label("Mail : "+utilisateur.getEmail());
        info.getChildren().addAll(nom, mail);
        info.setPadding(new Insets(5));
        info.setPadding(new Insets(10));
        infoPane.setCenter(info);

        HBox boutons = new HBox();
        
        Button buttonModifMdp = new Button("Modifier le mot de passe");
        buttonModifMdp.setStyle("-fx-background-radius: 20px;-fx-background-color: #FFFFFF;");
        buttonModifMdp.setOnMouseEntered(event -> {
            buttonModifMdp.setStyle("-fx-background-radius: 20px;-fx-background-color: #A47f29;");
        });
        buttonModifMdp.setOnMouseExited(event -> {
            buttonModifMdp.setStyle("-fx-background-radius: 20px;-fx-background-color: #FFFFFF;");
        });

        Button buttonRevoc = new Button("Révoquer l'accès");
        buttonRevoc.setStyle("-fx-background-radius: 20px;-fx-background-color: #FFFFFF;");
        buttonRevoc.setOnMouseEntered(event -> {
            buttonRevoc.setStyle("-fx-background-radius: 20px;-fx-background-color: #A47f29;");
        });
        buttonRevoc.setOnMouseExited(event -> {
            buttonRevoc.setStyle("-fx-background-radius: 20px;-fx-background-color: #FFFFFF;");
        });
        boutons.getChildren().addAll(buttonModifMdp, buttonRevoc);
        boutons.setSpacing(20);

        infoPane.setBottom(boutons);
        infoPane.getBottom().setStyle("-fx-padding: 5 38 5 5;");
        infoPane.setBorder(new Border(new BorderStroke(Color.GREY,BorderStrokeStyle.SOLID,null,null)));


        ImageView imageView = new ImageView(new Image("file:img/utilisateur.png"));
        imageView.setFitHeight(100);
        imageView.setFitWidth(100);

        boxUtilisateur.setSpacing(50);
        boxUtilisateur.setPadding(new Insets(20, 75, 20 ,75));
        boxUtilisateur.getChildren().addAll(imageView,infoPane);

        return boxUtilisateur;
    }

    private HBox getRectangle(){
        HBox hbox = new HBox();
        Rectangle ligne = new Rectangle();
        ligne.setStroke(Color.web("#B5B2B3")); // Set the color of the border
        ligne.setStrokeWidth(3); // Set the border width
        ligne.setWidth(this.width/2);
        ligne.setFill(null);
        hbox.getChildren().add(ligne);
        hbox.setAlignment(Pos.CENTER);
        return hbox;
    }
    
}