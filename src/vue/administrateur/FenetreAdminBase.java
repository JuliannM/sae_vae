package vue.administrateur;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;

import java.util.Arrays;
import java.util.List;

import vue.*;

public class FenetreAdminBase extends BorderPane {

    private double width;
    private double height;

    private ApplicationVAE app;

    protected Button btnEnchere;
    protected Button btnVente;
    protected Button btnFavori;
    protected Button btnDiscussion;

    public FenetreAdminBase(ApplicationVAE app, double width, double height) {
        super();

        this.width = width;
        this.height = height;
        this.app = app;

        // Top de la BorderPane
        VBox vboxTOP = new VBox();
        Background couleur = new Background(new BackgroundFill(Color.valueOf("#1F1E1E"), null, null));
        vboxTOP.setBackground(couleur);
        this.setTop(vboxTOP);

        // Haut de la VBox
        BorderPane borderPaneTOP = new BorderPane();
        // Button boutonMenu = new Button();
        // boutonMenu.setBackground(couleur);
        // boutonMenu.setGraphic(new ImageView(new Image("file:img/menu.png", 35, 35, false, false)));
        // borderPaneTOP.setLeft(boutonMenu);

        StackPane logo = new StackPane();
        logo.setPadding(new Insets(20));
        ImageView imgLogo = new ImageView(new Image("file:img/logo.jpeg", 160, 160, false, false));
        logo.getChildren().add(imgLogo);
        borderPaneTOP.setCenter(logo);

        HBox boxText = new HBox();
        Text message = new Text("Administrateur");
        message.setFont(Font.font("Arial", FontWeight.BOLD, 25));
        message.setFill(Color.WHITE);
        boxText.getChildren().add(message);
        boxText.setAlignment(Pos.CENTER);
        boxText.setPadding(new Insets(20));
        borderPaneTOP.setBottom(boxText);

        Button boutonCagnotte = new Button();
        boutonCagnotte.setBackground(couleur);
        boutonCagnotte.setGraphic(new ImageView(new Image("file:img/cagnotte.png", 50, 50, false, false)));
        borderPaneTOP.setRight(boutonCagnotte);

        vboxTOP.getChildren().add(borderPaneTOP);

    
        // Bottom de la BorderPane
        VBox vboxBOT = new VBox();
        vboxBOT.setBackground(couleur);
        vboxBOT.setSpacing(10);
        vboxBOT.setAlignment(Pos.TOP_CENTER);
        vboxBOT.setMaxHeight(100);
        vboxBOT.setMinHeight(100);

        Label messageReseau = new Label("Nos Réseaux :");
        messageReseau.setFont(Font.font("Arial", FontWeight.NORMAL, 12));
        messageReseau.setTextFill(Color.WHITE);
        messageReseau.setPadding(new Insets(10, 0, 0, 0));

        HBox reseaux = new HBox();
        List<String> nomImg = Arrays.asList("Facebook", "In", "Insta", "Twitter", "Yt");
        for (String nomImage : nomImg) {
            ImageView imgReseau = new ImageView(new Image("file:img/reseaux/" + nomImage + ".png", 35, 30, false, true));
            reseaux.getChildren().add(imgReseau);
        }

        reseaux.setPadding(new Insets(0, 10, 10, 10));
        reseaux.setSpacing(10);
        reseaux.setAlignment(Pos.CENTER);
        vboxBOT.getChildren().addAll(messageReseau, reseaux);

        this.setBottom(vboxBOT);
    }
}
