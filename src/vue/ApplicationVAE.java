package vue;

import modele.UtilisateurDB;
import modele.ConnexionMySQL;

import vue.accueil.*;
import vue.perso.*;
import vue.identification.*;
import vue.administrateur.*;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.text.Font;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.geometry.Pos;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;

import java.util.List;


import controleur.ControleurAccueil;
import controleur.ControleurBoutonDeconnexion;
import controleur.ControleurBoutonEspacePerso;

import java.sql.SQLException;
import java.util.Arrays;
import javafx.util.Duration;
import javafx.animation.TranslateTransition;
import javafx.scene.text.FontWeight;
import modele.*;
import vue.administrateur.*;
import controleur.*;

public class ApplicationVAE extends Application{
    
    private Scene scene;
    private Stage stage;

    private double width;
    private double height;

    private UtilisateurDB utilisateurDB;
    private ConnexionMySQL connexionMySQL;
    private VenteDB venteDB;
    private CategorieDB categorieDB;

    private FenetreConnexion fenetreConnexion;
    private FenetreCreationCompte fenetreCreationCompte;
    private FenetreIdentification fenetreIdentification;
    private FenetreFavoris fenetreFavoris;
    private FenetreAjouteObjet fenetreAjouteObjet;

    private HBox rootH;
    private VBox sidebar;
    private StackPane sp;
    private boolean isSidebarOpen = false;
    private Button menu;
    
    @Override
    public void init(){
        // récupère la taille de l'ecran
        Rectangle2D visualBounds = Screen.getPrimary().getVisualBounds();
        this.width = visualBounds.getWidth();
        this.height = visualBounds.getHeight();
        try{
            this.connexionMySQL = new ConnexionMySQL();
        }catch (ClassNotFoundException ex){
            System.out.println("Driver MySQL non trouvé!!! "+ex.getMessage());
            System.exit(1);
        }
        try{
            this.connexionMySQL.connecter();
        }catch(SQLException e){
            System.out.println("Connection a la BD non réussi "+e.getMessage());
            System.exit(1);
        }

        this.utilisateurDB = new UtilisateurDB(this.connexionMySQL);
        this.venteDB = new VenteDB(this.connexionMySQL);
        this.categorieDB = new CategorieDB(this.connexionMySQL);

        this.rootH = new HBox();
        this.menu = createToggleButton();
        this.sidebar = createMenu();
        this.sp = new StackPane();
        this.sp.setAlignment(this.menu, Pos.TOP_LEFT);

        this.fenetreConnexion = new FenetreConnexion(this, this.width, this.height);
        this.fenetreCreationCompte = new FenetreCreationCompte(this, this.width, this.height);
        this.fenetreIdentification = new FenetreIdentification(this, this.width, this.height);
        this.fenetreAjouteObjet = new FenetreAjouteObjet(this, this.width, this.height);
        this.fenetreFavoris = new FenetreFavoris(this, this.width, this.height);
    }

    public FenetreConnexion getFenetreConnexion(){
        return this.fenetreConnexion;
    }

    public FenetreCreationCompte getFenetreCreationCompte(){
        return this.fenetreCreationCompte;
    }

    public FenetreIdentification getFenetreIdentification(){
        return this.fenetreIdentification;
    }

    public FenetreAjouteObjet getFenetreAjouteObjet(){
        return this.fenetreAjouteObjet;
    }
    
    @Override
    public void start(Stage stage){
        this.stage = stage;

        FenetreIdentification root = new FenetreIdentification(this, this.width, this.height);

        this.scene = new Scene(root, this.width, this.height);

        //this.scene = new Scene(this.fenetreFavoris, this.width, this.height);
        
        this.stage.setResizable(false);
        this.stage.setScene(scene);
        this.stage.setTitle("Application VAE");
        this.stage.show();    
    }
 
    public void afficheIdentification(){
        this.scene.setRoot(this.fenetreIdentification);
    }

    public void afficheCreationCompte(){
        this.scene.setRoot(this.fenetreCreationCompte);
    }

    public void afficheConnexion(){
        this.scene.setRoot(this.fenetreConnexion);
    }

    public void afficheEnchere(){
        this.rootH = new HBox();
        this.sp = new StackPane();
        this.sp.setAlignment(this.menu, Pos.TOP_LEFT);
        this.sp.setMinWidth(this.width);
        this.sidebar = this.createMenu();

        FenetreEnchere root = new FenetreEnchere(this, this.width, this.height);
        this.rootH.getChildren().addAll(this.menu, this.sp);
        this.sp.getChildren().addAll(root, this.menu);
        this.scene.setRoot(this.rootH);
    }

    public void afficheAjouteObjet(){
        this.rootH = new HBox();
        this.sp = new StackPane();
        this.sp.setAlignment(this.menu, Pos.TOP_LEFT);
        this.sp.setMinWidth(this.width);
        this.sidebar = this.createMenu();

        this.rootH.getChildren().addAll(this.menu, this.sp);
        this.sp.getChildren().addAll(this.fenetreAjouteObjet, this.menu);
        this.scene.setRoot(this.rootH);
    }

    public void afficheVente(){
        this.rootH = new HBox();
        this.sp = new StackPane();
        this.sp.setAlignment(this.menu, Pos.TOP_LEFT);
        this.sp.setMinWidth(this.width);
        this.sidebar = this.createMenu();

        FenetreVente root = new FenetreVente(this, this.width, this.height);
        this.rootH.getChildren().addAll(this.menu, this.sp);
        this.sp.getChildren().addAll(root, this.menu);
        this.scene.setRoot(this.rootH);
    }

    public void afficheFavoris(){
        this.rootH = new HBox();
        this.sp = new StackPane();
        this.sp.setAlignment(this.menu, Pos.TOP_LEFT);
        this.sp.setMinWidth(this.width);
        this.sidebar = this.createMenu();

        FenetreFavoris root = new FenetreFavoris(this, this.width, this.height);
        this.rootH.getChildren().addAll(this.menu, this.sp);
        this.sp.getChildren().addAll(root, this.menu);
        this.scene.setRoot(this.rootH);
    }

    public void afficheDiscussion(){
        this.rootH = new HBox();
        this.sp = new StackPane();
        this.sp.setAlignment(this.menu, Pos.TOP_LEFT);
        this.sp.setMinWidth(this.width);
        this.sidebar = this.createMenu();

        FenetreDiscussion root = new FenetreDiscussion(this, this.width, this.height);
        this.rootH.getChildren().addAll(this.menu, this.sp);
        this.sp.getChildren().addAll(root, this.menu);
        this.scene.setRoot(this.rootH);
    }


    public void afficheAcc(){
        this.rootH = new HBox();
        this.sp = new StackPane();
        this.sp.setAlignment(this.menu, Pos.TOP_LEFT);
        this.sp.setMinWidth(this.width);
        this.sidebar = this.createMenu();
        
        FenetreAccueil root = new FenetreAccueil(this, this.width, this.height);
        this.rootH.getChildren().addAll(this.menu, this.sp);
        this.sp.getChildren().addAll(root, this.menu);
        this.scene.setRoot(rootH);
    }

    public void afficheAdministrateurUser(){
        this.rootH = new HBox();
        this.sp = new StackPane();
        this.sp.setAlignment(this.menu, Pos.TOP_LEFT);
        this.sp.setMinWidth(this.width);
        this.sidebar = this.createMenuAdmin();
        
        FenetreAdminUser root = new FenetreAdminUser(this, this.width, this.height);
        this.rootH.getChildren().addAll(this.menu, this.sp);
        this.sp.getChildren().addAll(root, this.menu);
        this.scene.setRoot(rootH);
    }

    public void afficheAdministrateurVente(){
        this.rootH = new HBox();
        this.sp = new StackPane();
        this.sp.setAlignment(this.menu, Pos.TOP_LEFT);
        this.sp.setMinWidth(this.width);
        this.sidebar = this.createMenuAdmin();
        
        FenetreAdminVente root = new FenetreAdminVente(this, this.width, this.height);
        this.rootH.getChildren().addAll(this.menu, this.sp);
        this.sp.getChildren().addAll(root, this.menu);
        this.scene.setRoot(rootH);
    }

    public void afficheAcc(int categorie){
        this.rootH = new HBox();
        this.sp = new StackPane();
        this.sp.setAlignment(this.menu, Pos.TOP_LEFT);
        this.sp.setMinWidth(this.width);
        this.sidebar = this.createMenu();
        
        FenetreAccueil root = new FenetreAccueil(this, this.width, this.height, categorie);
        this.rootH.getChildren().addAll(this.menu, this.sp);
        this.sp.getChildren().addAll(root, this.menu);
        this.scene.setRoot(rootH);
    }

    public void afficheAcc(String nom){
        this.rootH = new HBox();
        this.sp = new StackPane();
        this.sp.setAlignment(this.menu, Pos.TOP_LEFT);
        this.sp.setMinWidth(this.width);
        this.sidebar = this.createMenu();

        FenetreAccueil root = new FenetreAccueil(this, this.width, this.height, nom);
        this.rootH.getChildren().addAll(this.menu, this.sp);
        this.sp.getChildren().addAll(root, this.menu);
        this.scene.setRoot(rootH);
    }

    public UtilisateurDB getUtilisateurDB(){
        return this.utilisateurDB;
    }

    public VenteDB getVenteDB(){
        return this.venteDB;
    }

    public ConnexionMySQL getConnexionMySQL(){
        return this.connexionMySQL;
    }

    public CategorieDB getCategorieDB(){
        return this.categorieDB;
    }

    public Button getMenu(){
        return this.menu;
    }

    private Button createToggleButton() {
        this.menu = new Button();
        this.menu.setBackground(new Background(new BackgroundFill(Color.valueOf("#1F1E1E"), null, null)));
        this.menu.setGraphic(new ImageView(new Image("file:img/menu.png", 30, 30, false, false)));
        this.menu.setOnAction(e -> {
            if (isSidebarOpen) {
                closeSidebar();
            } else {
                openSidebar();
            }
        });
        return this.menu;
    }

    private void openSidebar() {
        TranslateTransition openTransition = new TranslateTransition(Duration.seconds(0.3), this.sidebar);
        openTransition.setToX(0);
        openTransition.play();

        isSidebarOpen = true;
        rootH.getChildren().add(0, this.sidebar);
    }

    private void closeSidebar() {
        TranslateTransition closeTransition = new TranslateTransition(Duration.seconds(0.3), this.sidebar);
        closeTransition.setToX(-200);
        closeTransition.play();

        isSidebarOpen = false;
        rootH.getChildren().remove(this.sidebar);
    }

    private VBox createMenu() {
        VBox vbox = new VBox();
        vbox.setStyle("-fx-background-color: white;");
        vbox.setPrefWidth(200);
        vbox.setMinWidth(200);
        vbox.setMaxWidth(200);
        vbox.setTranslateX(-200);
        Button accueil = new Button("Accueil");
        accueil.setOnAction(new ControleurAccueil(this));
        accueil.setFont(Font.font("Arial", FontWeight.BOLD, 15));
        Button monEspace = new Button("Mon Espace");
        monEspace.setOnAction(new ControleurBoutonEspacePerso(this));
        monEspace.setFont(Font.font("Arial", FontWeight.BOLD, 15));
        Button aide = new Button("Aide");
        aide.setFont(Font.font("Arial", FontWeight.BOLD, 15));
        Button deconnexion = new Button("Déconnexion");
        deconnexion.setOnAction(new ControleurBoutonDeconnexion(this));
        deconnexion.setFont(Font.font("Arial", FontWeight.BOLD, 15));
        
        Button ajouteObj = new Button("Ajoute vente");
        ajouteObj.setFont(Font.font("Arial", FontWeight.BOLD, 15));
        ajouteObj.setId("ajouteV");
        ajouteObj.setOnAction(new ControleurBoutonAjouteV(this));

        List<Button> lesBoutons = Arrays.asList(accueil ,monEspace, deconnexion, ajouteObj, aide);

        for(Button b : lesBoutons){
            b.setPrefWidth(200);
            b.setStyle("-fx-background-color: white;");
            b.setPadding(new Insets(20));

        }
        vbox.getChildren().addAll(accueil, monEspace, deconnexion, ajouteObj, aide);
        return vbox;
    }

    private VBox createMenuAdmin() {
        VBox vbox = new VBox();
        vbox.setStyle("-fx-background-color: white;");
        vbox.setPrefWidth(200);
        vbox.setMinWidth(200);
        vbox.setMaxWidth(200);
        vbox.setTranslateX(-200);

        Button utilisateur = new Button("Admin Utilisateur");
        utilisateur.setOnAction(new ControleurBoutonAdmin(this));
        utilisateur.setFont(Font.font("Arial", FontWeight.BOLD, 15));
        utilisateur.setId("adminUser");

        Button vente = new Button("Admin Vente");
        vente.setOnAction(new ControleurBoutonAdmin(this));
        vente.setFont(Font.font("Arial", FontWeight.BOLD, 15));
        vente.setId("adminVente");

        Button deconnexion = new Button("Déconnexion");
        deconnexion.setOnAction(new ControleurBoutonDeconnexion(this));
        deconnexion.setFont(Font.font("Arial", FontWeight.BOLD, 15));

        List<Button> lesBoutons = Arrays.asList(utilisateur, vente, deconnexion);

        for(Button b : lesBoutons){
            b.setPrefWidth(200);
            b.setStyle("-fx-background-color: white;");
            b.setPadding(new Insets(20));

        }
        vbox.getChildren().addAll(utilisateur, vente, deconnexion);
        return vbox;
    }
}
