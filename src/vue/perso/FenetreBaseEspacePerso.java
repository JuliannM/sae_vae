package vue.perso;

import javafx.geometry.Insets;
import javafx.geometry.Pos;

import javafx.scene.control.Button;
import javafx.scene.control.Label;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;

import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;
import controleur.*;
import vue.*;

import javafx.scene.shape.Rectangle;
import javafx.scene.control.Tooltip;

public class FenetreBaseEspacePerso extends BorderPane {

    private double width;
    private double height;
    
    private ApplicationVAE app;

    protected Button btnEnchere;
    protected Button btnVente;
    protected Button btnFavori;
    protected Button btnDiscussion;

    public FenetreBaseEspacePerso(ApplicationVAE app, double width, double height) {
        super();

        this.width = width;
        this.height = height;
        this.app = app;

        this.btnEnchere = this.getBoutonEnchere();
        this.btnVente = this.getBoutonVente();
        this.btnFavori = this.getBoutonFavoris();
        this.btnDiscussion = this.getBoutonDiscussion();

        BorderPane top = new BorderPane();
        top.setMaxWidth(width);
        top.setMinWidth(width);
        top.setMaxHeight(height*0.30);
        top.setMinHeight(height*0.30);

        BorderPane topText = new BorderPane();
        Label text = new Label("MON ESPACE PERSONNEL : ");
        text.setFont(Font.font("Arial", FontWeight.BOLD, 30));
        text.setTextFill(Color.WHITE);
        text.setPadding(new Insets(25));
        text.setStyle("-fx-border-color: #A47f29; -fx-border-width: 0 0 3px 0;");
        topText.setCenter(text);
        topText.setBackground(new Background(new BackgroundFill(Color.valueOf("#1F1E1E"), null, null)));


        // mise en place de la CAGNOTTE à droite

        Button boutonCagnotte = new Button();
        boutonCagnotte.setBackground(new Background(new BackgroundFill(Color.valueOf("#1F1E1E"), null, null)));
        ImageView imgCagnotte = new ImageView(new Image("file:img/cagnotte.png"));
        imgCagnotte.setFitHeight(50);
        imgCagnotte.setFitWidth(50);
        boutonCagnotte.setGraphic(imgCagnotte);
        topText.setRight(boutonCagnotte);
        boutonCagnotte.setTooltip(new Tooltip("Mon Compte : € 38.12\nDépense Total : € 114\nObjet En Vente : 0\nObjet Encheri : 3"));

        topText.setPadding(new Insets(0, 0, 15, 0));

        //topText.setLeft(this.app.getMenu());

        top.setTop(topText);

        HBox hboxBOT = new HBox();
        
        // creation du cadre avec l'image de profil

        StackPane profil = new StackPane();
        profil.setPadding(new Insets(15, 50, 15, 30));
        ImageView imgProfil = new ImageView(new Image("file:img/profil.png"));
        imgProfil.setFitWidth(125);
        imgProfil.setFitHeight(125);
        profil.getChildren().add(imgProfil);



        // Ajout et placement de la hbox et du titre dans la borderPane


        hboxBOT.getChildren().addAll(profil, this.getVboxInfo());
        hboxBOT.setPadding(new Insets(15));

        StackPane logo = new StackPane();
        logo.setPadding(new Insets(10, 15, 5, 10));
        ImageView imgLogo = new ImageView(new Image("file:img/logo.jpeg"));
        imgLogo.setFitWidth(50);
        imgLogo.setFitHeight(50);
        logo.setAlignment(Pos.TOP_CENTER);
        logo.getChildren().addAll(imgLogo);

        // mise en place de la vbox du reseau

        HBox bottom = new HBox();

        VBox vboxBOT = new VBox();
        vboxBOT.setSpacing(10);
        vboxBOT.setAlignment(Pos.TOP_CENTER);

        Label messageReseau = new Label("Nos Réseaux :");
        messageReseau.setFont(Font.font("Arial", FontWeight.NORMAL, 12));
        messageReseau.setTextFill(Color.WHITE);
        messageReseau.setPadding(new Insets(10, 0, 0, 0));

        HBox reseaux = new HBox();
        List<String> nomImg = Arrays.asList("Facebook", "In", "Insta", "Twitter", "Yt");
        for (String nom : nomImg) {
            ImageView imgReseau = new ImageView(new Image("file:img/reseaux/" + nom + ".png"));
            imgReseau.setFitWidth(30);
            imgReseau.setFitHeight(25);
            reseaux.getChildren().add(imgReseau);
        }

        reseaux.setPadding(new Insets(0, 10, 5, 10));
        reseaux.setSpacing(10);
        reseaux.setAlignment(Pos.TOP_CENTER);
        vboxBOT.getChildren().addAll(messageReseau, reseaux);
                
        bottom.getChildren().addAll(logo, vboxBOT);
        bottom.setAlignment(Pos.TOP_CENTER);
        bottom.setBackground(new Background(new BackgroundFill(Color.valueOf("#1F1E1E"), null, null)));

        bottom.setMaxHeight(100);
        bottom.setMinHeight(100);

        this.setTop(top);
        top.setLeft(hboxBOT);
        top.setCenter(this.getBoutons());
        top.setBottom(this.getRectangle());
        this.setBottom(bottom);
    }

    private VBox getVboxInfo(){
        VBox vboxInfo = new VBox();
        vboxInfo.setSpacing(10);
        BackgroundFill backgroundFill = new BackgroundFill(Color.WHITE, new CornerRadii(5), Insets.EMPTY);
        Background background = new Background(backgroundFill);
        vboxInfo.setBackground(background);

        Text infoTitle = new Text("Mes informations :");
        infoTitle.setFont(Font.font("Arial", FontWeight.BOLD, 16));
        infoTitle.setFill(Color.BLACK);

        String pseudo = this.app.getFenetreConnexion().getPseudo();
        Text pseudoText = new Text("Pseudo :" + pseudo);
        pseudoText.setFont(Font.font("Arial", FontWeight.NORMAL, 12));
        pseudoText.setFill(Color.BLACK);

        String mdp = this.app.getFenetreConnexion().getMdp();
        Text mdpText = new Text("Mot de passe :" + mdp);
        mdpText.setFont(Font.font("Arial", FontWeight.NORMAL, 12));
        mdpText.setFill(Color.BLACK);
        try{
        
        String mail = this.app.getUtilisateurDB().getEmailPseudo(this.app.getFenetreConnexion().getPseudo());
        Text mailText = new Text("Mail :" + mail);
        mailText.setFont(Font.font("Arial", FontWeight.NORMAL, 12));
        mailText.setFill(Color.BLACK);
        vboxInfo.getChildren().addAll(infoTitle, pseudoText, mdpText, mailText);
        }catch(SQLException e){
            System.out.println(e);
        }

        // ajout et placement des élements dans la vbox


        vboxInfo.setMinWidth(this.width / 3);
        vboxInfo.setMaxWidth(this.width / 2);
        vboxInfo.setPadding(new Insets(20, 10, 10, 10));

        return vboxInfo;
    }

    private GridPane getBoutons(){

        GridPane gridPane = new GridPane();
        gridPane.setHgap(100);
        gridPane.setVgap(this.height/17);
        gridPane.setPadding(new Insets(15));
        gridPane.setAlignment(Pos.CENTER);


        // gridPane.setHalignment(this.btnEnchere, HPos.CENTER);
        // gridPane.setHalignment(this.btnVente, HPos.CENTER);
        // gridPane.setHalignment(this.btnFavori, HPos.CENTER);
        // gridPane.setHalignment(this.btnDiscussion, HPos.CENTER);

        gridPane.add(this.btnEnchere, 0, 0);
        gridPane.add(this.btnVente, 1, 0);
        gridPane.add(this.btnFavori, 0, 1);
        gridPane.add(this.btnDiscussion, 1, 1);

        return gridPane;
    }

    private Button getBoutonEnchere(){
        Button btnEnchere = new Button("ENCHÈRES");
        btnEnchere.setFont(Font.font("Arial", FontWeight.BOLD, 16));
        btnEnchere.setTextFill(Color.BLACK);
        btnEnchere.setPrefWidth(this.width/6);
        btnEnchere.setOnAction(new ControleurCategorie(this.app));
        btnEnchere.setStyle("-fx-background-radius: 20px;-fx-background-color: #FFFFFF;");
        return btnEnchere;
    }

    private Button getBoutonVente(){
        Button btnVente = new Button("VENTES");
        btnVente.setFont(Font.font("Arial", FontWeight.BOLD, 16));
        btnVente.setTextFill(Color.BLACK);
        btnVente.setPrefWidth(this.width/6);
        btnVente.setOnAction(new ControleurCategorie(this.app));
        btnVente.setStyle("-fx-background-radius: 20px;-fx-background-color: #FFFFFF;");
        return btnVente;
    }

    private Button getBoutonFavoris(){
        Button btnFavori = new Button("FAVORIS");
        btnFavori.setFont(Font.font("Arial", FontWeight.BOLD, 16));
        btnFavori.setTextFill(Color.BLACK);
        btnFavori.setPrefWidth(this.width/6);
        btnFavori.setOnAction(new ControleurCategorie(this.app));
        btnFavori.setStyle("-fx-background-radius: 20px;-fx-background-color: #FFFFFF;");
        return btnFavori;
    }

    private Button getBoutonDiscussion(){
        Button btnDiscussion = new Button("DISCUSSIONS");
        btnDiscussion.setFont(Font.font("Arial", FontWeight.BOLD, 16));
        btnDiscussion.setTextFill(Color.BLACK);
        btnDiscussion.setPrefWidth(this.width/6);
        btnDiscussion.setOnAction(new ControleurCategorie(this.app));
        btnDiscussion.setStyle("-fx-background-radius: 20px;-fx-background-color: #FFFFFF;");
        return btnDiscussion;
    }

    private Rectangle getRectangle(){
        Rectangle ligne = new Rectangle();
        ligne.setStroke(Color.web("#A47f29")); // Définition de la couleur de la bordure
        ligne.setStrokeWidth(5); // Définition de l'épaisseur de la bordure
        ligne.setWidth(this.width);
        ligne.setFill(null);
        return ligne;
    }
}