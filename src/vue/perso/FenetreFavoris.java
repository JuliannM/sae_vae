package vue.perso;

import javafx.geometry.Insets;

import javafx.scene.control.*;
import javafx.scene.layout.*;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import vue.*;


public class FenetreFavoris extends FenetreBaseEspacePerso{

    public FenetreFavoris(ApplicationVAE vae, double width, double height){
        super(vae, width, height);

        // VBOX CENTER

        int col = 0;
        int ligne = 0;

        GridPane gp = new GridPane();
        gp.setVgap(50);
        gp.setHgap(150);
        gp.setPadding(new Insets(50, 50, 50, 50));

        for(int i=0 ; i<15 ; i++){
            HBox hb1 = new HBox();
            ImageView viewTable = new ImageView(new Image("file:img/table.png", 125, 125, false, false));
            GridPane infoT = new GridPane();
            infoT.setStyle("-fx-background-color: #faf5ef;");
            Label nomT = new Label("Table");
            nomT.setPadding(new Insets(5));
            Label descT = new Label("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris tristique aliquam\nnunc, eget bibendum arcu volutpat vel. Donec ultrices enim vulputate, consequat\ntortor quis, posuere lorem.");
            descT.setPadding(new Insets(5));
            Label prixT = new Label("Prix : 85 €");
            prixT.setPadding(new Insets(5));
            infoT.add(nomT, 0, 0);
            infoT.add(descT, 0, 1);
            infoT.add(prixT, 0, 2);
            HBox.setMargin(infoT, new Insets(5, 5, 5, 20));
            infoT.setGridLinesVisible(true);
            hb1.getChildren().addAll(viewTable, infoT);
            hb1.setPadding(new Insets(10));

            gp.add(hb1, col, ligne);

            if(col == 1){
                col = 0;
                ligne++;
            }else{
                col++;
            }
        }

        // RIGHT

        this.setRight(new Filtre(vae));


        // SCROLLPANE

        ScrollPane scrollPane = new ScrollPane(gp);
        scrollPane.setFitToWidth(true);
        this.setCenter(scrollPane);

        this.btnEnchere.setOnMouseEntered(event -> {
            this.btnEnchere.setStyle("-fx-background-radius: 20px;-fx-background-color: #A47f29;");
        });
        this.btnEnchere.setOnMouseExited(event -> {
            this.btnEnchere.setStyle("-fx-background-radius: 20px;-fx-background-color: #FFFFFF;");
        });

        this.btnVente.setOnMouseEntered(event -> {
            this.btnVente.setStyle("-fx-background-radius: 20px;-fx-background-color: #A47f29;");
        });
        this.btnVente.setOnMouseExited(event -> {
            this.btnVente.setStyle("-fx-background-radius: 20px;-fx-background-color: #FFFFFF;");
        });

        this.btnDiscussion.setOnMouseEntered(event -> {
            this.btnDiscussion.setStyle("-fx-background-radius: 20px;-fx-background-color: #A47f29;");
        });
        this.btnDiscussion.setOnMouseExited(event -> {
            this.btnDiscussion.setStyle("-fx-background-radius: 20px;-fx-background-color: #FFFFFF;");
        });

        this.btnFavori.setStyle("-fx-background-radius: 20px;-fx-background-color: #A47f29;");
    }
}