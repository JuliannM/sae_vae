package vue.perso;

import javafx.geometry.Insets;

import javafx.scene.control.ScrollPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import java.util.ArrayList;

import java.util.List;

import vue.*;


public class FenetreDiscussion extends FenetreBaseEspacePerso{

    public FenetreDiscussion(ApplicationVAE vae, double width, double height) {
        super(vae, width, height);

        VBox vboxCenter = new VBox();
        vboxCenter.setSpacing(20); // Espacement de 20 pixels entre les éléments
        vboxCenter.setPadding(new Insets(10)); // Padding tout autour de 10 pixels

        // Création d'une liste avec le nombre de discussions qu'il y aura

        List<HBox> listMessage = new ArrayList<>();

        // Création des HBox des messages

        for (int i = 1; i < 10; i++) { // 5 représente un nombre aléatoire (à changer par la méthode qui récupère le nombre de messages)
            HBox hboxMessage = new HBox();
            if(i != 1){
                hboxMessage.setStyle("-fx-border-color: #A47f29; -fx-border-width: 2 0 0 0;");
            }
            VBox vboxMessage = new VBox();

            Text nomUtilisateur = new Text("Emmanuel Macron");
            nomUtilisateur.setFont(Font.font("Arial", FontWeight.BOLD, 14));
            nomUtilisateur.setFill(Color.BLACK);

            Text message = new Text("aaaaaaaaaaaaaaaaaaaaaa");
            message.setFont(Font.font("Arial", FontWeight.NORMAL, 12));
            message.setFill(Color.BLACK);

            vboxMessage.getChildren().addAll(nomUtilisateur, message);
            vboxMessage.setPadding(new Insets(10, 10, 10, 25));

            StackPane profilMessage = new StackPane();
            profilMessage.setPadding(new Insets(15));
            ImageView img = new ImageView(new Image("file:img/profil.png", 50, 50, false, false));
            profilMessage.getChildren().add(img);

            hboxMessage.getChildren().addAll(img, vboxMessage);

            hboxMessage.setPadding(new Insets(10));
            listMessage.add(hboxMessage);
        }

        vboxCenter.getChildren().addAll(listMessage);
        
        // SCROLLPANE

        ScrollPane scrollPane = new ScrollPane(vboxCenter);
        scrollPane.setFitToWidth(true);
        scrollPane.setFitToHeight(true);
        scrollPane.setPadding(new Insets(0));
        this.setCenter(scrollPane);

        this.btnEnchere.setOnMouseEntered(event -> {
            this.btnEnchere.setStyle("-fx-background-radius: 20px;-fx-background-color: #A47f29;");
        });
        this.btnEnchere.setOnMouseExited(event -> {
            this.btnEnchere.setStyle("-fx-background-radius: 20px;-fx-background-color: #FFFFFF;");
        });

        this.btnVente.setOnMouseEntered(event -> {
            this.btnVente.setStyle("-fx-background-radius: 20px;-fx-background-color: #A47f29;");
        });
        this.btnVente.setOnMouseExited(event -> {
            this.btnVente.setStyle("-fx-background-radius: 20px;-fx-background-color: #FFFFFF;");
        });

        this.btnFavori.setOnMouseEntered(event -> {
            this.btnFavori.setStyle("-fx-background-radius: 20px;-fx-background-color: #A47f29;");
        });
        this.btnFavori.setOnMouseExited(event -> {
            this.btnFavori.setStyle("-fx-background-radius: 20px;-fx-background-color: #FFFFFF;");
        });

        this.btnDiscussion.setStyle("-fx-background-radius: 20px;-fx-background-color: #A47f29;");

    }
}
