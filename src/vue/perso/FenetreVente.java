package vue.perso;

import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;

import javafx.scene.layout.*;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javafx.scene.image.ImageView;
import javafx.scene.image.Image;

import modele.*;
import vue.*;
import vue.perso.*;

public class FenetreVente extends FenetreBaseEspacePerso{

    public FenetreVente(ApplicationVAE vae, double width, double height){
        super(vae, width, height);

        // VBOX CENTER

        List<Pane> listFavoris = new ArrayList<>();
        List<Objet> lesObjets = new ArrayList<>();
        List<Double> lesprix = new ArrayList<>();
        try{
            lesObjets = vae.getVenteDB().getVente(vae.getFenetreConnexion().getPseudo());
            lesprix = vae.getVenteDB().getPrix(vae.getFenetreConnexion().getPseudo());
        }catch(SQLException e){}
        int j=0;
        // Création des HBox des messages
        for (Objet unObjet : lesObjets) { // 5 représente un nombre aléatoire (à changer par la méthode qui récupère le nombre de messages)
            Pane vuObjet = this.objetVu(unObjet,false,lesprix.get(j));
            
            listFavoris.add(vuObjet);
            j++;
        }

        GridPane grid = new GridPane();

        grid.addRow(0);
        grid.addRow(1);

        grid.addColumn(0);
        grid.addColumn(1);

        grid.setHgap(50);
        grid.setVgap(50);
        // ...
        grid.addColumn(2); // Ajouter une nouvelle colonne

        grid.setHgap(25); // Réduire l'espacement horizontal

        for (int i = 0; i < listFavoris.size(); i++) {
            Pane objetPane = listFavoris.get(i);
            grid.add(objetPane, i % 2, i / 2); // Utiliser i/2 pour l'indice de ligne et i%2 pour l'indice de colonne
        
            // Ajuster la largeur de l'objet pour qu'il prenne la moitié de la largeur de la colonne
            objetPane.setPrefWidth(width / 2);
        }

        // SCROLLPANE

        ScrollPane scrollPane = new ScrollPane(grid);
        scrollPane.setFitToWidth(true);
        this.setCenter(scrollPane);

        this.btnEnchere.setOnMouseEntered(event -> {
            this.btnEnchere.setStyle("-fx-background-radius: 20px;-fx-background-color: #A47f29;");
        });
        this.btnEnchere.setOnMouseExited(event -> {
            this.btnEnchere.setStyle("-fx-background-radius: 20px;-fx-background-color: #FFFFFF;");
        });

        this.btnFavori.setOnMouseEntered(event -> {
            this.btnFavori.setStyle("-fx-background-radius: 20px;-fx-background-color: #A47f29;");
        });
        this.btnFavori.setOnMouseExited(event -> {
            this.btnFavori.setStyle("-fx-background-radius: 20px;-fx-background-color: #FFFFFF;");
        });

        this.btnDiscussion.setOnMouseEntered(event -> {
            this.btnDiscussion.setStyle("-fx-background-radius: 20px;-fx-background-color: #A47f29;");
        });
        this.btnDiscussion.setOnMouseExited(event -> {
            this.btnDiscussion.setStyle("-fx-background-radius: 20px;-fx-background-color: #FFFFFF;");
        });

        this.btnVente.setStyle("-fx-background-radius: 20px;-fx-background-color: #A47f29;");
    }

    private Pane objetVu(Objet objet, boolean like, Double prixObjet) {
        HBox root = new HBox();
        //Gestion des images superposées
        File path = new File("img/");

        // load source images
        BufferedImage image = null;
        java.awt.Image imageN = null;
        try {
            image = ImageIO.read(new File(path, "chaise.jpg"));
            imageN = image.getScaledInstance(135, 135, java.awt.Image.SCALE_DEFAULT);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        BufferedImage overlay = null;
        try {
            if (like) {
                overlay = ImageIO.read(new File(path, "heart.png"));
            } else {
                overlay = ImageIO.read(new File(path, "noheart.png"));
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        // create the new image, canvas size is the max. of both image sizes
        int w = overlay.getWidth();
        int h = overlay.getHeight();
        BufferedImage combined = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);

        // paint both images, preserving the alpha channels
        Graphics g = combined.getGraphics();
        g.drawImage(imageN, 0, 0, null);
        g.drawImage(overlay, 0, 0, null);

        g.dispose();

        // Save as new image
        try {
            if (like) {
                ImageIO.write(combined, "PNG", new File(path, objet.getNom() + "_liked.png"));
            } else {
                ImageIO.write(combined, "PNG", new File(path, objet.getNom() + "_no_liked.png"));
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }


        // Choix de l'image
        ImageView imageView = null;
        if (like) {
            imageView = new ImageView("img/"+objet.getNom() + "_liked.png");
        } else {
            imageView = new ImageView("img/"+objet.getNom() + "_no_liked.png");
        }


        // Gestion des infos
        HBox hb1 = new HBox();
        GridPane infoT = new GridPane();
        infoT.setStyle("-fx-background-color: #faf5ef;");
        Label nomT = new Label(objet.getNom());
        nomT.setPadding(new Insets(5));
        Label descT = new Label(objet.getDescription());
        descT.setPadding(new Insets(5));
        descT.setWrapText(true);
        descT.setMaxHeight(65);

        BorderPane prix = new BorderPane();
        prix.setPadding(new Insets(5));

        Label prixT = new Label("Prix : " + prixObjet + " €");
        prixT.setPadding(new Insets(5));
        prix.setLeft(prixT);

        ImageView message = new ImageView(new Image("file:img/info.png"));
        message.setFitWidth(25);
        message.setFitHeight(25);
        prix.setRight(message);


        infoT.add(nomT, 0, 0);
        infoT.add(descT, 0, 1);
        infoT.add(prix, 0, 2);
        HBox.setMargin(infoT, new Insets(5, 5, 5, 20));
        infoT.setGridLinesVisible(true);
        hb1.getChildren().addAll(imageView, infoT);
        hb1.setPadding(new Insets(10));

        root.setSpacing(50);
        root.setPadding(new Insets(20));
        root.getChildren().add(hb1);
        return root;
    }
}
