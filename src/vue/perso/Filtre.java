package vue.perso;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Slider;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;

import controleur.ControleurFiltrer;

import vue.ApplicationVAE;

public class Filtre extends VBox{

    public Filtre(ApplicationVAE app){
        this.setStyle("-fx-border-color: #A47f29; -fx-border-width: 0 0 0 3;");

        // bouton trier
        Button trier = new Button("Trier");
        trier.setPrefWidth(111);
        trier.setId("trier");
        trier.setStyle("-fx-background-radius: 20px;-fx-background-color: #A47F29;");
        trier.setFont(Font.font("Arial", FontWeight.BOLD, 15));
        trier.setOnAction(new ControleurFiltrer(app));

        // bouton filtrer
        Button filtrer = new Button("Filtrer");
        filtrer.setId("filtrer");
        filtrer.setPrefWidth(111);
        filtrer.setStyle("-fx-background-radius: 20px;-fx-background-color: #A47F29;");
        filtrer.setFont(Font.font("Arial", FontWeight.BOLD, 15));
        filtrer.setOnAction(new ControleurFiltrer(app));

        // slider prix
        GridPane grillePrix = new GridPane();

        ImageView logoPrix = new ImageView(new Image("file:img/Prix.png"));
        logoPrix.setFitWidth(28);
        logoPrix.setFitHeight(28);
        Text prix = new Text("Prix");
        prix.setFont(Font.font("Arial", FontWeight.BOLD, 14));
        Button min = new Button("Min");
        min.setId("min");
        min.setOnAction(new ControleurFiltrer(app));
        min.setPrefWidth(50);
        min.setStyle("-fx-background-radius: 20px;-fx-background-color: #A47F29;");
        min.setFont(Font.font("Arial", FontWeight.BOLD, 9));
        Button max = new Button("Max");
        max.setId("max");
        max.setOnAction(new ControleurFiltrer(app));
        max.setPrefWidth(50);
        max.setStyle("-fx-background-radius: 20px;-fx-background-color: #A47F29;");
        max.setFont(Font.font("Arial", FontWeight.BOLD, 9));
        Slider slider = new Slider(0, 10000, 10000);
        slider.setShowTickMarks(true);
        slider.setMajorTickUnit(5000);
        slider.setMinorTickCount(4);
        slider.setShowTickLabels(true);

        
        // int valeurDuSlider = (int) slider.getValue();


        grillePrix.add(logoPrix, 0, 0, 1, 1);
        grillePrix.add(prix, 1, 0);
        grillePrix.add(min, 1, 1);
        grillePrix.add(max, 10, 1);
        grillePrix.add(slider, 1, 2, 10, 1);

        grillePrix.setPadding(new Insets(5));
        grillePrix.setHgap(10);
        grillePrix.setVgap(10);

        // checkbox type de vendeur
        GridPane grilleVendeur = new GridPane();

        ImageView logoVendeur = new ImageView(new Image("file:img/Vendeur.png"));
        logoVendeur.setFitWidth(28);
        logoVendeur.setFitHeight(28);
        Text typeDeVendeurs = new Text("Type de vendeurs");
        typeDeVendeurs.setFont(Font.font("Arial", FontWeight.BOLD, 14));
        CheckBox checkBoxParticuliers = new CheckBox("Particuliers");
        CheckBox checkBoxProfessionnels = new CheckBox("Professionnels");

        grilleVendeur.add(logoVendeur, 0, 0, 1, 1);
        grilleVendeur.add(typeDeVendeurs, 1, 0);
        grilleVendeur.add(checkBoxParticuliers, 1, 2);
        grilleVendeur.add(checkBoxProfessionnels, 1, 3);

        grilleVendeur.setPadding(new Insets(5));
        grilleVendeur.setHgap(10);
        grilleVendeur.setVgap(10);

        // datePicker date
        GridPane grilleDate = new GridPane();

        ImageView logoCalendrier = new ImageView(new Image("file:img/Calendrier.png"));
        logoCalendrier.setFitWidth(31);
        logoCalendrier.setFitHeight(31);
        Text date = new Text("Date");
        date.setFont(Font.font("Arial", FontWeight.BOLD, 14));

        grilleDate.add(logoCalendrier, 0,0,1,1);
        grilleDate.add(date, 1, 0);
        grilleDate.add(new DatePicker(), 1, 2, 3, 3);

        grilleDate.setPadding(new Insets(5));
        grilleDate.setHgap(10);
        grilleDate.setVgap(10);

        // Boutons effacer et Rechercher
        HBox deuxDerniersBoutons = new HBox();

        Button effacer = new Button("Effacer");
        effacer.setId("effacer");
        effacer.setOnAction(new ControleurFiltrer(app));

        effacer.setPrefWidth(140);
        effacer.setStyle("-fx-background-radius: 20px;-fx-background-color: #A47F29;");
        effacer.setFont(Font.font("Arial", FontWeight.BOLD, 15));

        Button rechercher = new Button("Rechercher");
        rechercher.setId("rechercher");
        rechercher.setOnAction(new ControleurFiltrer(app));
        rechercher.setPrefWidth(140);
        rechercher.setStyle("-fx-background-radius: 20px;-fx-background-color: #A47F29;");
        rechercher.setFont(Font.font("Arial", FontWeight.BOLD, 15));


        deuxDerniersBoutons.setPadding(new Insets(5));
        deuxDerniersBoutons.setSpacing(20);
        deuxDerniersBoutons.getChildren().addAll(effacer, rechercher);
        deuxDerniersBoutons.setAlignment(Pos.CENTER);

        this.setPadding(new Insets(11));
        this.setSpacing(14);
        this.getChildren().addAll(trier, filtrer, grillePrix, grilleVendeur, grilleDate, deuxDerniersBoutons);
        this.setAlignment(Pos.CENTER);
    }
}