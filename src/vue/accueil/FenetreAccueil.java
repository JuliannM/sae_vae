package vue.accueil;

import javafx.geometry.Insets;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.scene.control.ScrollPane;


import java.sql.*;

import vue.ApplicationVAE;
import modele.Vente;
import vue.*;
import vue.perso.*;

public class FenetreAccueil extends FenetreBaseAccueil{

    private double width;
    private double height;
    private ApplicationVAE app;
    private int ligne;
    private int col;

    public FenetreAccueil(ApplicationVAE app, double width, double height){
        this(app, width, height, 1);
    }

    public FenetreAccueil(ApplicationVAE app, double width, double height, int categorie) {
        super(app, width, height, categorie);

        this.width = width;
        this.height = height;
        this.app = app;

        GridPane gp = new GridPane();
        gp.setVgap(50);
        gp.setHgap(150);
        gp.setPadding(new Insets(50, 50, 50, 50));

        try {
            for (Vente v : app.getVenteDB().getVentesCategorie(10, this.categorie)) {
                HBox hb1 = new HBox();
                //ImageView viewTable = new ImageView(new Image("file:img/table.png", 125, 125, false, false));
                VueImageObjet viewTable = new VueImageObjet("img/table.png");
                viewTable.setImageObjet(v.getObjet().getImage());

                GridPane infoT = new GridPane();
                infoT.setStyle("-fx-background-color: #faf5ef;");
                Label nomT = new Label(v.getObjet().getNom());
                nomT.setPadding(new Insets(5));
                Label descT = new Label(v.getObjet().getDescription());
                descT.setPadding(new Insets(5));
                Label prixT = new Label(v.getObjet().getID() + "");
                prixT.setPadding(new Insets(5));
                infoT.add(nomT, 0, 0);
                infoT.add(descT, 0, 1);
                infoT.add(prixT, 0, 2);
                HBox.setMargin(infoT, new Insets(5, 5, 5, 20));
                infoT.setGridLinesVisible(true);
                hb1.getChildren().addAll(viewTable, infoT);
                hb1.setPadding(new Insets(10));

                Button bouton = new Button("Voir plus");
                infoT.add(bouton, 0, 3);
                bouton.setMinWidth(100);
                bouton.setMaxWidth(100);
                bouton.setStyle("-fx-background-color: #A47F29;");
                bouton.setId(v.getIdVente() + "");

                gp.add(hb1, this.col, this.ligne);

                if (this.col == 1) {
                    this.col = 0;
                    this.ligne++;
                } else {
                    this.col++;
                }
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    public FenetreAccueil(ApplicationVAE app, double width, double height, String nom){
        super(app, width, height, 1);

        this.width = width;
        this.height = height;
        this.app = app;

        GridPane gp = new GridPane();
        gp.setVgap(50);
        gp.setHgap(150);
        gp.setPadding(new Insets(50, 50, 50, 50));

        try {
            for (Vente v : app.getVenteDB().getVentesNom(10, nom)) {
                HBox hb1 = new HBox();
                //ImageView viewTable = new ImageView(new Image("file:img/table.png", 125, 125, false, false));
                VueImageObjet viewTable = new VueImageObjet("img/table.png");
                viewTable.setImageObjet(v.getObjet().getImage());

                GridPane infoT = new GridPane();
                infoT.setStyle("-fx-background-color: #faf5ef;");
                Label nomT = new Label(v.getObjet().getNom());
                nomT.setPadding(new Insets(5));
                Label descT = new Label(v.getObjet().getDescription());
                descT.setPadding(new Insets(5));
                Label prixT = new Label(v.getObjet().getID() + "");
                prixT.setPadding(new Insets(5));
                infoT.add(nomT, 0, 0);
                infoT.add(descT, 0, 1);
                infoT.add(prixT, 0, 2);
                HBox.setMargin(infoT, new Insets(5, 5, 5, 20));
                infoT.setGridLinesVisible(true);
                hb1.getChildren().addAll(viewTable, infoT);
                hb1.setPadding(new Insets(10));

                Button bouton = new Button("Voir plus");
                infoT.add(bouton, 0, 3);
                bouton.setMinWidth(100);
                bouton.setMaxWidth(100);
                bouton.setStyle("-fx-background-color: #A47F29;");
                bouton.setId(v.getIdVente() + "");

                gp.add(hb1, this.col, this.ligne);

                if (this.col == 1) {
                    this.col = 0;
                    this.ligne++;
                } else {
                    this.col++;
                }
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

        this.setCenter(gp);

        this.setRight(new Filtre(app));

        // SCROLL

        ScrollPane scrollPane = new ScrollPane(gp);
        scrollPane.setFitToWidth(true);
        this.setCenter(scrollPane);
    }
}