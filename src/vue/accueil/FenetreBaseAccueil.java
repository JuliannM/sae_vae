package vue.accueil;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import controleur.*;
import util.Style;
import vue.*;
import vue.perso.*;
import modele.*;
import java.sql.*;

public class FenetreBaseAccueil extends BorderPane {

    private double width;
    private double height;

    private int ligne;
    private int col;

    protected int categorie;   

    public FenetreBaseAccueil(ApplicationVAE app, double width, double height, int categorie){
        super();

        this.categorie = categorie;

        // background haut de page 
        VBox vboxTOP = new VBox();
        Background couleur = new Background(new BackgroundFill(Color.valueOf(Style.background), null, null));
        vboxTOP.setBackground(couleur);
        this.setTop(vboxTOP);

        // Haut de la VBox
        BorderPane borderPaneTOP = new BorderPane();

        StackPane logo = new StackPane();
        logo.setPadding(new Insets(20));
        ImageView imgLogo = new ImageView(new Image("file:img/logo.jpeg"));
        imgLogo.setFitWidth(160);
        imgLogo.setFitHeight(160);
        logo.getChildren().add(imgLogo);
        borderPaneTOP.setCenter(logo);

        //barre de recherche 


        //creation de la VBox pour la barre de recherche 

        BorderPane borderSearch = new BorderPane();
        borderSearch.setPadding((new Insets(0,0,50,0)));
        HBox hboxSearchbar = new HBox();
        hboxSearchbar.setMaxWidth(550);
        hboxSearchbar.setStyle("-fx-border-radius: 20; -fx-border-color: #1F1E1E; -fx-border-width: 5; -fx-padding: 5; -fx-background-color: white; -fx-background-insets: 7;");
        hboxSearchbar.setAlignment(Pos.CENTER);//mettre au centre
        //permet de gérer les bords arrondis

        //creation du textField 

        TextField searchbar = new TextField();
        searchbar.setPromptText("Faites vos recherches...");
        searchbar.setBackground(new Background(new BackgroundFill(Color.WHITE, null, null)));
        searchbar.setOnKeyReleased(new ControleurRecherche(app, searchbar));
        searchbar.setPrefWidth(500);

        hboxSearchbar.getChildren().add(searchbar);

        //button recherche 
        Button research = new Button();
        research.setBackground(new Background(new BackgroundFill(Color.WHITE, null, null)));
        research.setGraphic(new ImageView(new Image("file:img/loupe.png", 18, 18, false, false)));
        research.setOnAction(new ControleurRechercheBouton(app, searchbar));
        hboxSearchbar.getChildren().add(research);
        
        borderSearch.setCenter(hboxSearchbar);


        //Cagntotte 

        Button boutonCagnotte = new Button();
        boutonCagnotte.setBackground(couleur);
        boutonCagnotte.setGraphic(new ImageView(new Image("file:img/cagnotte.png", 50, 50, false, false)));
        boutonCagnotte.setTooltip(new Tooltip("Mon Compte : € 38.12\nDépense Total : € 114\nObjet En Vente : 0\nObjet Encheri : 3"));
        borderPaneTOP.setRight(boutonCagnotte);


        HBox hboxBOT = new HBox();
        
        // creation du cadre avec l'image de profil

        StackPane profil = new StackPane();
        profil.setPadding(new Insets(15, 50, 15, 30));
        ImageView imgProfil = new ImageView(new Image("file:img/profil.png", 125, 125, false, false));
        profil.getChildren().add(imgProfil);
        hboxBOT.setPadding(new Insets(15));

        // ajout et placement dans une hbox puis dans la borderPane
        
        BorderPane borderBOT = new BorderPane();
        borderBOT.setCenter(hboxBOT);
        BorderPane.setMargin(hboxBOT, new Insets(0, 0, 0, 0));

        vboxTOP.getChildren().addAll(borderPaneTOP, borderBOT, borderSearch);


        // VBOX CENTER


        VBox vboxCenter = new VBox();
        vboxCenter.setSpacing(20); // Espacement de 20 pixels entre les éléments
        vboxCenter.setPadding(new Insets(10)); // Padding tout autour de 10 pixels
        this.setCenter(vboxCenter);

        // Ajout d'une barre de défilement à la vboxCenter

        HBox hboxCategorie = new HBox();
        hboxCategorie.setSpacing(10);

        List<Categorie> listCategorie = new ArrayList<>();

        try{
            listCategorie = app.getCategorieDB().getCategories();
        }catch(SQLException e){
            System.out.println(e.getMessage());
        }
        List<Button> listButton = new ArrayList<>();

        for (Categorie cat : listCategorie) {
            Button button = new Button(cat.getNomCategorie());
            button.setFont(Font.font("Arial", FontWeight.BOLD, 16));
            button.setTextFill(Color.BLACK);
            button.setStyle("-fx-background-radius: 20px;-fx-background-color: "+Style.rgbBouton+";");
            button.setMaxWidth(Double.MAX_VALUE);
            button.setOnAction(new ControleurBouton(app, cat.getIdCategorie()));

            if (!(this.categorie == cat.getIdCategorie())){
                button.setStyle("-fx-background-radius: 20px;-fx-background-color: "+Style.blanc+";");
                button.setOnMouseEntered(event -> {
                    button.setStyle("-fx-background-radius: 20px;-fx-background-color: "+Style.rgbBouton+";");
                });
                button.setOnMouseExited(event -> {
                    button.setStyle("-fx-background-radius: 20px;-fx-background-color: "+Style.blanc+";");
                });
            }

            HBox.setHgrow(button, Priority.ALWAYS);

            listButton.add(button);
        }

        hboxCategorie.getChildren().addAll(listButton);
        vboxCenter.getChildren().add(hboxCategorie);
        vboxCenter.setBackground(new Background(new BackgroundFill(Color.WHITESMOKE, null, null)));
        vboxTOP.getChildren().add(vboxCenter);

        // bottom (nos réseaux + logos)
        VBox vboxBOT = new VBox();
        vboxBOT.setBackground(new Background(new BackgroundFill(Color.valueOf(Style.background), null, null)));
        vboxBOT.setSpacing(10);
        vboxBOT.setAlignment(Pos.TOP_CENTER);
        vboxBOT.setMaxHeight(100);
        vboxBOT.setMinHeight(100);
        
        Label messageReseau = new Label("Nos Réseaux :");
        messageReseau.setFont(Font.font("Arial", FontWeight.NORMAL, 12));
        messageReseau.setTextFill(Color.WHITE);
        messageReseau.setPadding(new Insets(10, 0, 0, 0));

        HBox reseaux = new HBox();
        List<String> nomImg = Arrays.asList("Facebook", "In", "Insta", "Twitter", "Yt");
        for (String nom : nomImg) {
            ImageView imgReseau = new ImageView(new Image("file:img/reseaux/" + nom + ".png", 35, 30, false, true));
            reseaux.getChildren().add(imgReseau);
        }

        reseaux.setPadding(new Insets(0, 10, 10, 10));
        reseaux.setSpacing(10);
        reseaux.setAlignment(Pos.CENTER);
        vboxBOT.getChildren().addAll(messageReseau, reseaux);
        this.setBottom(vboxBOT);
    }
}






