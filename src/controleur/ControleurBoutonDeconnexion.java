package controleur;


import javafx.event.ActionEvent;
import javafx.event.EventHandler;

import vue.ApplicationVAE;

public class ControleurBoutonDeconnexion implements EventHandler<ActionEvent>{
    
    ApplicationVAE app;

    public ControleurBoutonDeconnexion(ApplicationVAE app) {
        this.app = app;
    }

    @Override
    public void handle(ActionEvent actionEvent) {
        this.app.afficheIdentification();
    }
}
