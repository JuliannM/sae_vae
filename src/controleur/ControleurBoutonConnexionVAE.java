package controleur;

import java.sql.SQLException;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import modele.ConnexionMySQL;
import modele.UtilisateurDB;

import vue.ApplicationVAE;
import vue.identification.FenetreConnexion;

import vue.alert.AlertConnexion;
import vue.*;

import java.io.ByteArrayOutputStream;
import java.awt.image.BufferedImage;
import java.io.File;
import javax.imageio.ImageIO;
import java.sql.*;

public class ControleurBoutonConnexionVAE implements EventHandler<ActionEvent>{
    
    ApplicationVAE app;

    public ControleurBoutonConnexionVAE(ApplicationVAE app) {
        this.app = app;
    }

    @Override
    public void handle(ActionEvent actionEvent) {
        UtilisateurDB utilisateurDB = this.app.getUtilisateurDB();
        ConnexionMySQL mySQL = this.app.getConnexionMySQL();
        FenetreConnexion fenetreConnexion = this.app.getFenetreConnexion();

        try {
            mySQL.connecter();
            if(utilisateurDB.verifierEtConnecterUtilisateur(fenetreConnexion.getPseudo(), fenetreConnexion.getMdp())){

                if(utilisateurDB.estAdmin(fenetreConnexion.getPseudo())){
                    this.app.afficheAdministrateurUser();
                }else{
                    this.app.afficheAcc();
                }

                //try{
                //    BufferedImage bImage = ImageIO.read(new File("img/hajar.png"));
                //    ByteArrayOutputStream bos = new ByteArrayOutputStream();
                //    ImageIO.write(bImage, "png", bos );
                //    byte [] data = bos.toByteArray();

                //    PreparedStatement ps2 = mySQL.prepareStatement("insert into IMAGE_OBJET (idob, img) values (?,?)");
                //    ps2.setInt(1, 1);
                //    Blob b=mySQL.getMysql().createBlob();
                //    b.setBytes(1,data);
                //    ps2.setBlob(2,b);
                //    ps2.executeUpdate();

                //}catch(Exception e){
                //    System.out.println(e.getMessage());
                //}
                
                
            }else{
                AlertConnexion alert = new AlertConnexion();
            } 
        }catch (SQLException e) {
            System.out.println("Erreur lors de la connexion à la base de données: " + e.getMessage());
            System.exit(1);
        }
    }
}
