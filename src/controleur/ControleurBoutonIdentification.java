package controleur;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;


import vue.ApplicationVAE;

import javafx.scene.control.Button;

public class ControleurBoutonIdentification implements EventHandler<ActionEvent> {

    private ApplicationVAE app;

    public ControleurBoutonIdentification(ApplicationVAE app){
        this.app = app;
    }

    /**
     * @param actionEvent
     */
    @Override
    public void handle(ActionEvent actionEvent) {
        Button bouton = (Button)actionEvent.getTarget();
        String id = bouton.getId();

        switch(id){
            case "creation":
                this.app.afficheCreationCompte();
            break;
            case "connexion":
                this.app.afficheConnexion();
            break;
            case "retour":
                this.app.afficheIdentification();
            break;
        }
    }
}
