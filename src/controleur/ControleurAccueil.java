package controleur;


import javafx.event.ActionEvent;
import javafx.event.EventHandler;

import vue.ApplicationVAE;

public class ControleurAccueil implements EventHandler<ActionEvent>{
    
    ApplicationVAE app;

    public ControleurAccueil(ApplicationVAE app) {
        this.app = app;
    }

    @Override
    public void handle(ActionEvent actionEvent) {
        this.app.afficheAcc();
    }
}
