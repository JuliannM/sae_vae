package controleur;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;


import vue.ApplicationVAE;

import javafx.scene.control.Button;

public class ControleurBoutonAjouteV implements EventHandler<ActionEvent> {

    private ApplicationVAE app;

    public ControleurBoutonAjouteV(ApplicationVAE app){
        this.app = app;
    }

    /**
     * @param actionEvent
     */
    @Override
    public void handle(ActionEvent actionEvent) {
        Button bouton = (Button)actionEvent.getTarget();
        String id = bouton.getId();

        switch(id){
            case "ajouteV":
                this.app.afficheAjouteObjet();
            break;
        }
    }
}
