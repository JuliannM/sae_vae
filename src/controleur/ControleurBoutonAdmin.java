package controleur;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;


import vue.ApplicationVAE;

import javafx.scene.control.Button;

public class ControleurBoutonAdmin implements EventHandler<ActionEvent> {

    private ApplicationVAE app;

    public ControleurBoutonAdmin(ApplicationVAE app){
        this.app = app;
    }

    /**
     * @param actionEvent
     */
    @Override
    public void handle(ActionEvent actionEvent) {
        Button bouton = (Button)actionEvent.getTarget();
        String id = bouton.getId();

        switch(id){
            case "adminUser":
                this.app.afficheAdministrateurUser();
            break;
            case "adminVente":
                this.app.afficheAdministrateurVente();
            break;
        }
    }
}
