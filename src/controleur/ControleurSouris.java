package controleur;

import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;

public class ControleurSouris implements EventHandler<MouseEvent> {

    private Button bouton;

    public ControleurSouris(Button bouton) {
        this.bouton = bouton;
    }

    @Override
    public void handle(MouseEvent event) {
        if (event.getEventType() == MouseEvent.MOUSE_ENTERED) {
            bouton.setStyle("-fx-background-radius: 20px;-fx-background-color: #dacaa5;");
        } else if (event.getEventType() == MouseEvent.MOUSE_EXITED) {
            bouton.setStyle("-fx-background-radius: 20px;-fx-background-color: #A47F29;");
        }
    }
}
