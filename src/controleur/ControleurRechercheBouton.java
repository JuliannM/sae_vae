package controleur;


import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.TextField;
import vue.ApplicationVAE;

public class ControleurRechercheBouton implements EventHandler<ActionEvent> {

    private ApplicationVAE app;
    private TextField recherche;

    public ControleurRechercheBouton(ApplicationVAE app, TextField recherche){
        this.app = app;
        this.recherche = recherche;
    }

    @Override
    public void handle(ActionEvent event){
        this.app.afficheAcc(this.recherche.getText());
    }
}
