package controleur;

import javafx.event.EventHandler;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import vue.ApplicationVAE;
import javafx.scene.control.TextField;


public class ControleurRecherche implements EventHandler<KeyEvent> {

    private TextField recherche;
    private ApplicationVAE app;

    public ControleurRecherche(ApplicationVAE app, TextField recherche){
        this.recherche = recherche;
        this.app = app;
    }
    @Override
    public void handle(KeyEvent keyEvent) {
        if (keyEvent.getCode().equals(KeyCode.ENTER)){
            this.app.afficheAcc(this.recherche.getText());
        }
    }
}
