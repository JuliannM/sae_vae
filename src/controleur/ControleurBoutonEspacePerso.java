package controleur;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;


import vue.ApplicationVAE;

public class ControleurBoutonEspacePerso implements EventHandler<ActionEvent>{
    
    ApplicationVAE app;

    public ControleurBoutonEspacePerso(ApplicationVAE app) {
        this.app = app;
    }

    @Override
    public void handle(ActionEvent actionEvent) {
        this.app.afficheEnchere();

    }
}
