package controleur;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;


import vue.ApplicationVAE;

/**
 * Contrôleur à activer lorsque l'on clique sur le bouton rejouer ou Lancer une
 * partie
 */
public class ControleurCategorie implements EventHandler<ActionEvent> {
    /**
     * vue du jeu
     **/
    private ApplicationVAE appli;

    /**
     * @param modelePendu modèle du jeu
     * @param p           vue du jeu
     */
    public ControleurCategorie(ApplicationVAE vae) {
        this.appli = vae;
    }

    /**
     * L'action consiste à recommencer une partie. Il faut vérifier qu'il n'y a pas
     * une partie en cours
     * 
     * @param actionEvent l'événement action
     */
    @Override
    public void handle(ActionEvent actionEvent) {
        // A implémenter

        Button b = (Button) actionEvent.getSource();
        if (b.getText().equals("ENCH\u00C8RES")) {
            this.appli.afficheEnchere();
        }
        else if (b.getText().equals("VENTES")) {
            this.appli.afficheVente();
        }
        else if (b.getText().equals("FAVORIS")) {
            this.appli.afficheFavoris();
        }
        else if (b.getText().equals("DISCUSSIONS")) {
            this.appli.afficheDiscussion();
        }
    }
}
