package controleur;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;


import vue.ApplicationVAE;

/**
 * Contrôleur à activer lorsque l'on clique sur un des boutons dans
 * le filtre (trier, filtrer, effacer, rechercher)
 */
public class ControleurFiltrer implements EventHandler<ActionEvent> {

    /**
     * @param vae la vue
     */
    private ApplicationVAE appli;

    
    public ControleurFiltrer(ApplicationVAE appli){
        this.appli = appli;
    }

    /**
     * L'action consiste à action à faire pour chaque bouton
     * 
     * @param actionEvent l'événement action
     */
    @Override
    public void handle(ActionEvent actionEvent) {
        Button b = (Button) actionEvent.getTarget();
        String id = b.getId();
        switch(id){
            case "trier":
                System.out.println("trier");
                break;
            case "filtrer":
                System.out.println("filtrer");
                break;
            case "effacer":
                System.out.println("effacer");

                break;
            case "rechercher":
                System.out.println("rechercher");
                break;
            case "min":
                System.out.println("min");
                break;
            case "max":
                System.out.println("max");
                break;
        }
    }
}