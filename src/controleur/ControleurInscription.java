package controleur;

import vue.ApplicationVAE;
import modele.ConnexionMySQL;
import modele.UtilisateurDB;
import vue.identification.FenetreCreationCompte;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Alert;

import java.sql.SQLException;

import modele.Utilisateur;
import util.Verification;

public class ControleurInscription implements EventHandler<ActionEvent>{

    ApplicationVAE app;

    public ControleurInscription(ApplicationVAE app) {
        this.app = app;
    }

    @Override
    public void handle(ActionEvent actionEvent) {
        UtilisateurDB utilisateurDB = this.app.getUtilisateurDB();
        ConnexionMySQL connexionMySQL = this.app.getConnexionMySQL();
        FenetreCreationCompte f = this.app.getFenetreCreationCompte();

        try{
            connexionMySQL.connecter();

            boolean erreur = false;
            if(!Verification.email(f.getEmail())){
                erreur = true;
                f.setEmailLabel("*L'email n'est pas valide.");
            }   

            if(utilisateurDB.emailExiste(f.getEmail())){
                erreur = true;
                f.setEmailLabel("*L'email est déja utilisé.");
            }

            if(!Verification.motDePasse(f.getMotDePasse())){
                erreur = true;
                f.setMdpLabel("*Le mot de passe n'est pas valide.");
            }

            if(!f.getMotDePasse().equals(f.getMotDePasse2())){
                erreur = true;
                f.setMdp2Label("*Les deux mots de passe ne correspondent pas.");
            }

            if(utilisateurDB.pseudoExiste(f.getPseudo())){
                erreur = true;
                f.setPseudoLabel("*Le pseudo est déja utilisé.");
            }

            if(utilisateurDB.emailExiste(f.getEmail())){
                erreur = true;
                f.setEmailLabel("*L'email est déja utilisé.");
            }

            if(!erreur){
                Utilisateur u = new Utilisateur(0, f.getPseudo(), f.getEmail(), f.getMotDePasse(), 0);
             
                utilisateurDB.ajouterUtilisateur(u);

                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Valide");
                alert.setResizable(true);
                alert.setWidth(500);
                alert.setHeaderText("Les informations sont valides");
                alert.setContentText(f.getMotDePasse()+" "+f.getPseudo()+" "+f.getEmail());
                alert.showAndWait();
            }
            
        }catch(SQLException e){
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
			alert.setTitle("Information invalide");
			alert.setResizable(true);
			alert.setWidth(500);
			alert.setHeaderText("Les informations sont invalides");
			alert.setContentText("Voici le message envoyé par le serveur\n"+e.getMessage());
			alert.showAndWait();
        }
    }
}
