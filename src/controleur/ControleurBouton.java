package controleur;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;

import vue.accueil.*;
import vue.*;

public class ControleurBouton implements EventHandler<ActionEvent> {

    private ApplicationVAE app;
    private int categorie;


    public ControleurBouton(ApplicationVAE app, int categorie){
        this.app = app;
        this.categorie = categorie;
    }

    @Override
    public void handle(ActionEvent event) {
        this.app.afficheAcc(this.categorie);
    }
}
