package modele;

public class Categorie {


    private int idCategorie;
    private String nomCategorie;

    /*
     * créer une nouvelle catégorie
     * 
     * @param int idCategorie
     * @param String nomCategorie
     */
    public Categorie(int idCategorie, String nomCategorie){
        this.idCategorie = idCategorie;
        this.nomCategorie = nomCategorie;
    }

    /*
     * retourne l'id de la catégorie
     * 
     * @return int
     */
    public int getIdCategorie(){
        return this.idCategorie;
    }

    /*
     * retourne le nom de la catégorie
     * 
     * @return String
     */
    public String getNomCategorie(){
        return this.nomCategorie;
    }


    public void setIdCategorie(int idCategorie){
        this.idCategorie = idCategorie;
    }

    public void setNomCategorie(String nomCategorie){
        this.nomCategorie = nomCategorie;
    }
    
}
