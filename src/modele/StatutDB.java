package modele;

import java.sql.*;
public class StatutDB {
    ConnexionMySQL laConnexion;
	Statement st;

    public StatutDB(ConnexionMySQL laConnexion){
        this.laConnexion=laConnexion;
    }

    public int maxNumStatut() throws SQLException{
        st = laConnexion.createStatement();
        ResultSet resultat = st.executeQuery("Select IFNULL(Max(idSt), 0) leMax from STATUT");
        resultat.next();
        int res = resultat.getInt(1);
        resultat.close();
        return res;
    }


    public void supprimerStatut(int idSt) throws SQLException{
        PreparedStatement ps = laConnexion.prepareStatement(
                "DELETE FROM STATUT WHERE idSt = ?");
        ps.setInt(1, idSt);
        ps.executeUpdate();
        ps.close();
    };


    public ResultSet getAllStatut() throws SQLException{
        PreparedStatement ps = laConnexion.prepareStatement(
                "SELECT * FROM STATUT");
        ResultSet rs = ps.executeQuery();
        return rs;
    };
}
