package modele;

public class Statut{

    public int idSt;

    public static int aVenir = 0;
    public static int enCours = 1;
    public static int aValider = 2;
    public static int validee = 3;
    public static int nonConclue = 4;

    public Statut(int idSt){
        this.idSt = idSt;
    }

    public String getNomSt(int idSt){
        if(this.idSt == 0)
            return "a venir";
        else if(this.idSt == 1)
            return "en cours";
        else if(this.idSt == 2)
            return "a valider";
        else if(this.idSt == 3)
            return "validee";
        else if(this.idSt == 4)
            return "non conclue";
        return null;
    }

    public int getIdSt(){
        return this.idSt;
    }

    public void setIdSt(int idSt){
        this.idSt = idSt;
    }

    public boolean equals(Statut statut){
        if(this.idSt == statut.getIdSt())
            return true;
        return false;
    }

    
}