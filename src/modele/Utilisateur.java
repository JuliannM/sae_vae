package modele;

import java.util.List;
import java.util.ArrayList;

public class Utilisateur{

    private int id;
    private String pseudonyme;
    private String mail;
    private String mdp;
    private List<Objet> mesObjets;
    private List<Objet> favoris;
    private int cagnotte;
    private String activeUt;
    private int role;

    public Utilisateur(int id, String pseudonyme, String mail, String mdp, int cagnotte){
        this.id = id;
        this.pseudonyme = pseudonyme;
        this.mail = mail;
        this.mdp = mdp;
        this.mesObjets = new ArrayList<>();
        this.favoris = new ArrayList<>();
        this.cagnotte = cagnotte;
        this.activeUt = "O";
        this.role = 2;
    }

    public int getID(){
        return this.id;
    }

    public String getEmail(){
        return this.mail;
    }

    public String getPseudonyme(){
        return this.pseudonyme;
    }

    public String getMotDePasse(){
        return this.mdp;
    }


    public List<Objet> getFavoris(){
        return this.favoris;
    }

    public List<Objet> getMesObjets(){
        return this.mesObjets;
    }

    public boolean favoris(Objet obj){
        if(this.favoris.isEmpty()){
            return false;
        }

        for(Objet unObjet : this.favoris){
            if(obj == unObjet){
                return false;
            }else{
                this.favoris.add(obj);
                return true;
            }
        }
        return false;
    }

    public int getCagnotte(){
        return this.cagnotte;
    }

    public void setCagnotte(int cagnotte){
        this.cagnotte = cagnotte;
    }

    public String getActiveUt(){
        return this.activeUt;
    }

    public int getRole(){
        return this.role;
    }

    public void setPseudonyme(String pseudonyme){
        this.pseudonyme = pseudonyme;
    }

    public Utilisateur getUtilisateur(){
        return this;
    }
}