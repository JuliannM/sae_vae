package modele;

import java.util.Date;

public class Enchere{

    private double montant;
    private Date date;
    private int id;
    private Utilisateur encherisseur;

    /**
     * @param int identifiant de l'enchère
     * @param Utilisateur qui enchèri
     * @param double montant de l'enchère
     * @param String date de l'enchère
     */
    public Enchere(int id, Utilisateur encherisseur, double montant, Date date){
        this.id = id;
        this.montant = montant;
        this.date = date;
        this.encherisseur = encherisseur;
    }

    /**
     * @return int
     */
    public int getId(){
        return this.id;
    }

    /**
     * @return Date
     */
    public Date getDate(){
        return this.date;
    }

    /**
     * @return double
     */
    public double montant(){
        return this.montant;
    }

    /**
     * @param Utilisateur
     * @param double valeur
     * @return boolean
     */
    public boolean encherir(Utilisateur encherisseur, double value){
        if(this.montant < value){
            this.montant = value;
            this.encherisseur = encherisseur;
            return true;
        }
        return false;
    }

    /**
     * @return Utilisateur
     */
    public Utilisateur getEncherisseur(){
        return this.encherisseur;
    }
}