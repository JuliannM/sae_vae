package modele;

import java.sql.*;
import java.util.List;
import java.util.ArrayList;

public class CategorieDB{

    ConnexionMySQL laConnexion;
	Statement st;

    public CategorieDB(ConnexionMySQL laConnexion){
        this.laConnexion=laConnexion;
    }

    public List<Categorie> getCategories()throws SQLException{
        List<Categorie> categories = new ArrayList<>();

        PreparedStatement ps = this.laConnexion.prepareStatement("select * from CATEGORIE order by idcat limit ?");
        ps.setInt(1, 6);

        ResultSet result = ps.executeQuery();
        while(result.next()){
            categories.add(new Categorie(result.getInt("idcat"), result.getString("nomcat")));
        }
        return categories;
    }
}
