package modele;
import java.util.Date;

public class Vente{

    private int idVente;
    private double prixBase;
    private double prixMin;
    private Date debutVente;
    private Date finVente;
    private Objet objEnVente;
    private Statut statut;

    /**
     * @param int idVente
     * @param double prixBase
     * @param double prixMin
     * @param Date debutVente
     * @param Date finVente
     * @param Objet objet
     * @param Statut statut
     */

    public Vente(int idVente, double prixBase, double prixMin, Date debutVente, Date finVente, Objet obj){
        this.idVente = idVente;
        this.prixBase = prixBase;
        this.prixMin = prixMin;
        this.debutVente = debutVente;
        this.finVente = finVente;
        this.objEnVente = obj;
        this.statut = new Statut(0);
        
    }

    /**
     * @return int
     */

    public int getIdVente(){
        return this.idVente;
    }

    /**
     * @return double
     */

    public double getPrixBase(){
        return this.prixBase;
    }

    /**
     * @return double
     */

    public double getPrixMin(){
        return this.prixMin;

    }

    /**
     * @return Date
     */

    public Date getDebutVente(){
        return this.debutVente;

    }


    /**
     * @return Date
    */

    public Date getFinVente(){
        return this.finVente;

    }

    /**
     * @return Objet
     */

    public Objet getObjet(){
        return this.objEnVente;

    }

    /**
     * @return Statut
     */

    public Statut getStatut(){
        return this.statut;
    }

    /**
     * @param double prixBase
     */

    public void setPrixBase(double prixBase){
        this.prixBase = prixBase;

    }

    /**
     * @param double prixMin
     */

    public void setPrixMin(double prixMin){
        this.prixMin = prixMin;
    }

    /**
     * @param Date debutVente
     */

    public void setDebutVente(Date debutVente){
        this.debutVente = debutVente;

    }

    /**
     * @param Date finVente
     */

    public void setFinVente(Date finVente){
        this.finVente = finVente;
    }

    /**
     * @param Objet objet
     */

    public void setObjet(Objet objet){
        this.objEnVente = objet;
    }

    /**
     * @param Statut statut
     */

    public void setStatut(Statut statut){
        this.statut = statut;
    }

    /**
     * @param Utilisateur encherisseur
     * @param double montant
     * @return boolean
     */

    public boolean encherir(Utilisateur encherisseur, double montant){
        if(this.prixMin < montant){
            this.prixMin = montant;
            return true;
        }
        return false;
    }

    /**
     * @param Utilisateur encherisseur
     * @return boolean
     */

    public boolean validerEnchere(Utilisateur encherisseur){
        if(this.statut.getIdSt() == 1){
            this.statut.setIdSt(2);
            return true;
        }
        return false;
    }

    /**
     * @param Utilisateur encherisseur
     * @return boolean
     */

    public boolean annulerEnchere(Utilisateur encherisseur){
        if(this.statut.getIdSt() == 1){
            this.statut.setIdSt(4);
            return true;
        }
        return false;
    }

    /**
     * @param Utilisateur encherisseur
     * @return boolean
     */

    public boolean conclureEnchere(Utilisateur encherisseur){
        if(this.statut.getIdSt() == 2){
            this.statut.setIdSt(3);
            return true;
        }
        return false;

    }

    /**
     * @param Utilisateur encherisseur
     * @return boolean
     */

    public boolean nonConclureEnchere(Utilisateur encherisseur){
        if(this.statut.getIdSt() == 2){
            this.statut.setIdSt(4);
            return true;
        }
        return false;
    }

    /**
     * @param Utilisateur encherisseur
     * @return boolean
     */

    public boolean annulerVente(Utilisateur encherisseur){
        if(this.statut.getIdSt() == 0){
            this.statut.setIdSt(4);
            return true;
        }
        return false;
    }
}
