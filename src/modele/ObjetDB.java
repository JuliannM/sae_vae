package modele;

import java.sql.*;
public class ObjetDB {
    ConnexionMySQL laConnexion;
	Statement st;

    public ObjetDB(ConnexionMySQL laConnexion){
        this.laConnexion=laConnexion;
    }

    public int maxNumObjet() throws SQLException{
        st = laConnexion.createStatement();
        ResultSet resultat = st.executeQuery("Select IFNULL(Max(idObjet), 0) leMax from OBJET");
        resultat.next();
        int res = resultat.getInt(1);
        resultat.close();
        return res;
    }

    public void ajouterObjet(Objet e) throws  SQLException{
        PreparedStatement ps = laConnexion.prepareStatement(
                "INSERT INTO OBJET (idob,nomob,descriptionob,idut,idcat) VALUES (?,?,?,?,?,?,?)");
        int nouvNum = maxNumObjet()+1;
        ps.setInt(1, nouvNum);
        ps.setString(2, e.getNom());
        ps.setString(3, e.getDescription());
        ps.setInt(4, e.getCategorieID());
        ps.setInt(5, e.getCategorieID());
        ps.executeUpdate();

        PreparedStatement ps2 = laConnexion.prepareStatement("insert into IMAGE_OBJET (idob, img) values (?,?)");
        ps2.setInt(nouvNum);

        Blob b=laConnexion.createBlob();
		b.setBytes(1,e.getImage());
		ps2.setBlob(7,b);
        ps2.executeUpdate();
    };

    public void supprimerObjet(int idObjet) throws SQLException{
        PreparedStatement ps = laConnexion.prepareStatement(
                "DELETE FROM OBJET WHERE idObjet = ?");
        ps.setInt(1, idObjet);
        ps.executeUpdate();
        ps.close();
    };

    public void modifierObjet(Objet e) throws SQLException{
        PreparedStatement ps = laConnexion.prepareStatement(
                "UPDATE OBJET SET nomob = ?, descriptionob = ?, idut = ?, idcat = ? WHERE idObjet = ?");
        ps.setString(1, e.getNom());
        ps.setString(2, e.getDescription());
        ps.setInt(3, e.getProprietaireID());
        ps.setInt(4, e.getCategorieID());
        ps.setInt(5, e.getID());
        ps.executeUpdate();
        ps.close();
    };

    public ResultSet getAllObjet() throws SQLException{
        PreparedStatement ps = laConnexion.prepareStatement(
                "SELECT * FROM OBJET");
        ResultSet rs = ps.executeQuery();
        return rs;
    };

    public ResultSet getObjet(int idObjet) throws SQLException{
        PreparedStatement ps = laConnexion.prepareStatement(
                "SELECT * FROM OBJET WHERE idObjet = ?");
        ps.setInt(1, idObjet);
        ResultSet rs = ps.executeQuery();
        return rs;
    };

    public ResultSet getObjetByCategorie(int idCategorie) throws SQLException{
        PreparedStatement ps = laConnexion.prepareStatement(
                "SELECT * FROM OBJET WHERE idcat = ?");
        ps.setInt(1, idCategorie);
        ResultSet rs = ps.executeQuery();
        return rs;
    };

    public ResultSet getObjetByProprietaire(int idProprietaire) throws SQLException{
        PreparedStatement ps = laConnexion.prepareStatement(
                "SELECT * FROM OBJET WHERE idut = ?");
        ps.setInt(1, idProprietaire);
        ResultSet rs = ps.executeQuery();
        return rs;
    };

    public ResultSet getObjetByNom(String nom) throws SQLException{
        PreparedStatement ps = laConnexion.prepareStatement(
                "SELECT * FROM OBJET WHERE nomob = ?");
        ps.setString(1, nom);
        ResultSet rs = ps.executeQuery();
        return rs;
    };


    public ResultSet getObjetByNomAndProprietaire(String nom, int idProprietaire) throws SQLException{
        PreparedStatement ps = laConnexion.prepareStatement(
                "SELECT * FROM OBJET WHERE nomob = ? AND idut = ?");
        ps.setString(1, nom);
        ps.setInt(2, idProprietaire);
        ResultSet rs = ps.executeQuery();
        return rs;
    };

    public int getPrixObjet(int idObjet) throws SQLException{
        PreparedStatement ps = laConnexion.prepareStatement(
                "SELECT prixBase FROM OBJET natural join VENTE WHERE idObjet = ?");
        ps.setInt(1, idObjet);
        ResultSet rs = ps.executeQuery();
        rs.next();
        int prix = rs.getInt(1);
        rs.close();
        return prix;
    };
}
