package modele;

import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class Objet{ //extends ObjetBD{
    private int id;
    private String nomObjet;
    private String description;
    private Utilisateur proprietaire;
    private Categorie categorie;
    private int prix;
    private byte[] img;

    public Objet(int id, String nom, String description, Utilisateur proprietaire, Categorie cat, int prix, byte[] img) {
        this.id = id;
        this.nomObjet = nom;
        this.description = description;
        this.proprietaire = proprietaire;
        this.categorie = cat;
        this.prix = prix;
        this.img = img;
    }

    public byte[] getImage(){
        return this.img;
    }

    public int getID() {
        return this.id;
    }

    public String getNom() {
        return this.nomObjet;
    }

    public String getDescription() {
        return this.description;
    }

    public Utilisateur getProprietaire() {
        return this.proprietaire;
    }
    public int getProprietaireID() {
        return this.proprietaire.getID();
    }

    public Categorie getCategorie() {
        return this.categorie;
    }
    public int getCategorieID() {
        return this.categorie.getIdCategorie();
    }

    public int getPrix() {
        return this.prix;
    }


    public Pane objetVu(Objet objet, boolean like) {
        HBox root = new HBox();

        //Gestion des images superposées
        File path = new File("img/");

        // load source images
        BufferedImage image = null;
        java.awt.Image imageN = null;
        try {
            image = ImageIO.read(new File(path, "chaise.jpg"));
            imageN = image.getScaledInstance(135, 135, java.awt.Image.SCALE_DEFAULT);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        BufferedImage overlay = null;
        try {
            if (like) {
                overlay = ImageIO.read(new File(path, "heart.png"));
            } else {
                overlay = ImageIO.read(new File(path, "noheart.png"));
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        // create the new image, canvas size is the max. of both image sizes
        int w = overlay.getWidth();
        int h = overlay.getHeight();
        BufferedImage combined = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);

        // paint both images, preserving the alpha channels
        Graphics g = combined.getGraphics();
        g.drawImage(imageN, 0, 0, null);
        g.drawImage(overlay, 0, 0, null);

        g.dispose();

        // Save as new image
        try {
            if (like) {
                ImageIO.write(combined, "PNG", new File(path, objet.getNom() + "_liked.png"));
            } else {
                ImageIO.write(combined, "PNG", new File(path, objet.getNom() + "_no_liked.png"));
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }


        // Choix de l'image
        ImageView imageView = null;
        if (like) {
            imageView = new ImageView("img/"+objet.getNom() + "_liked.png");
        } else {
            imageView = new ImageView("img/"+objet.getNom() + "_no_liked.png");
        }


        // Gestion des infos
        HBox hb1 = new HBox();
        GridPane infoT = new GridPane();
        infoT.setStyle("-fx-background-color: #faf5ef;");
        javafx.scene.control.Label nomT = new javafx.scene.control.Label(objet.getNom());
        nomT.setPadding(new javafx.geometry.Insets(5));
        javafx.scene.control.Label descT = new javafx.scene.control.Label(objet.getDescription());
        descT.setPadding(new javafx.geometry.Insets(5));
        descT.setWrapText(true);
        descT.setMaxHeight(65);

        BorderPane prix = new BorderPane();
        prix.setPadding(new javafx.geometry.Insets(5));

        javafx.scene.control.Label prixT = new Label("Prix : 85 €");
        prixT.setPadding(new javafx.geometry.Insets(5));
        prix.setLeft(prixT);

        ImageView message = new ImageView(new javafx.scene.image.Image("file:img/info.png"));
        message.setFitWidth(25);
        message.setFitHeight(25);
        prix.setRight(message);


        infoT.add(nomT, 0, 0);
        infoT.add(descT, 0, 1);
        infoT.add(prix, 0, 2);
        HBox.setMargin(infoT, new javafx.geometry.Insets(5, 5, 5, 20));
        infoT.setGridLinesVisible(true);
        hb1.getChildren().addAll(imageView, infoT);
        hb1.setPadding(new javafx.geometry.Insets(10));

        root.setSpacing(50);
        root.setPadding(new Insets(20));
        root.getChildren().add(hb1);
        return root;
    }
}
