package modele;

import javafx.scene.control.*;
import javafx.scene.layout.*;

public class FicheUtilisateur extends GridPane{

    private Label titre;
    private TextField numUti;
    private TextField speudo;
    private TextField email;
    private TextField mdp;
    private TextField activeUt;
    private TextField role;
    private TextField service;
    

    private Button bouton;

    public void setNomBouton(String nomBouton) {
        this.bouton.setText(nomBouton);

    }

     public void setTitre(String titre) {
        this.titre.setText(titre);
    }

    public void setNumEmp(int numUti){
        this.numUti.setText(""+numUti);
    }

    public String getTitre(){
        return this.titre.getText();
    }


    // public Utilisateur getUtilisateur(){
    //     int id=-1;
    //     try {
    //         id = Integer.parseInt(this.numUti.getText());
    //     }catch (Exception e){}
    //     String speudo=this.speudo.getText();
    //     String email=this.email.getText();
    //     String mdp=this.mdp.getText();
    //     String activeUt=this.activeUt.getText();
    //     int role= 2;
    //     try {
    //         role = Integer.parseInt(this.role.getText());  
    //     }catch (Exception e){}
	//     return new Utilisateur(speudo,email,mdp,activeUt,role);
    // }

    void viderFiche(){
        this.numUti.setText("");
        this.speudo.setText("");
        this.email.setText("");
        this.mdp.setText("");
        this.activeUt.setText("");
        this.role.setText("");
        this.service.setText("");
    }

    public int getnumUti(){
        return Integer.parseInt(this.numUti.getText());
    }

}
