package modele;

import java.sql.*;
import java.util.List;
import java.util.ArrayList;

public class VenteDB {
    ConnexionMySQL laConnexion;
	Statement st;

    public VenteDB(ConnexionMySQL laConnexion){
        this.laConnexion=laConnexion;
    }

    public int maxNumVente() throws SQLException{
        st = laConnexion.createStatement();
        ResultSet resultat = st.executeQuery("Select IFNULL(Max(idVente), 0) leMax from VENTE");
        resultat.next();
        int res = resultat.getInt(1);
        resultat.close();
        return res;
    }

    public void ajouterVente(Vente e) throws  SQLException{
        PreparedStatement ps = laConnexion.prepareStatement(
                "INSERT INTO VENTE (idVente,prixBase,prixMin,debutVente,finVente,idObjet,idSt) VALUES (?,?,?,?,?,?,?)");
        int nouvNum = maxNumVente()+1;
        ps.setInt(1, nouvNum);
        ps.setDouble(2, e.getPrixBase());
        ps.setDouble(3, e.getPrixMin());
        ps.setDate(4, (Date) e.getDebutVente());
        ps.setDate(5, (Date) e.getFinVente());
        ps.setInt(6, e.getObjet().getID());
        ps.setInt(7, e.getStatut().getIdSt());
        ps.executeUpdate();
        ps.close();
    };

    public void supprimerVente(int idVente) throws SQLException{
        PreparedStatement ps = laConnexion.prepareStatement(
                "DELETE FROM VENTE WHERE idVente = ?");
        ps.setInt(1, idVente);
        ps.executeUpdate();
        ps.close();
    };

    public void modifierVente(Vente e) throws SQLException{
        PreparedStatement ps = laConnexion.prepareStatement(
                "UPDATE VENTE SET prixBase = ?, prixMin = ?, debutVente = ?, finVente = ?, idObjet = ?, idSt = ? WHERE idVente = ?");
        ps.setDouble(1, e.getPrixBase());
        ps.setDouble(2, e.getPrixMin());
        ps.setDate(3, (Date) e.getDebutVente());
        ps.setDate(4, (Date) e.getFinVente());
        ps.setInt(5, e.getObjet().getID());
        ps.setInt(6, e.getStatut().getIdSt());
        ps.setInt(7, e.getIdVente());
        ps.executeUpdate();
        ps.close();
    };

    public void modifierStatutVente(int idVente, int idSt) throws SQLException{
        PreparedStatement ps = laConnexion.prepareStatement(
                "UPDATE VENTE SET idSt = ? WHERE idVente = ?");
        ps.setInt(1, idSt);
        ps.setInt(2, idVente);
        ps.executeUpdate();
        ps.close();
    };

    public void modifierPrixVente(int idVente, double prix) throws SQLException{
        PreparedStatement ps = laConnexion.prepareStatement(
                "UPDATE VENTE SET prixBase = ? WHERE idVente = ?");
        ps.setDouble(1, prix);
        ps.setInt(2, idVente);
        ps.executeUpdate();
        ps.close();
    };

    public void modifierPrixMinVente(int idVente, double prix) throws SQLException{
        PreparedStatement ps = laConnexion.prepareStatement(
                "UPDATE VENTE SET prixMin = ? WHERE idVente = ?");
        ps.setDouble(1, prix);
        ps.setInt(2, idVente);
        ps.executeUpdate();
        ps.close();
    };

    public void modifierDebutVente(int idVente, Date date) throws SQLException{
        PreparedStatement ps = laConnexion.prepareStatement(
                "UPDATE VENTE SET debutVente = ? WHERE idVente = ?");
        ps.setDate(1, date);
        ps.setInt(2, idVente);
        ps.executeUpdate();
        ps.close();
    };

    public void modifierFinVente(int idVente, Date date) throws SQLException{
        PreparedStatement ps = laConnexion.prepareStatement(
                "UPDATE VENTE SET finVente = ? WHERE idVente = ?");
        ps.setDate(1, date);
        ps.setInt(2, idVente);
        ps.executeUpdate();
        ps.close();
    };

    public void modifierObjetVente(int idVente, int idObjet) throws SQLException{
        PreparedStatement ps = laConnexion.prepareStatement(
                "UPDATE VENTE SET idObjet = ? WHERE idVente = ?");
        ps.setInt(1, idObjet);
        ps.setInt(2, idVente);
        ps.executeUpdate();
        ps.close();
    };

    public List<Vente> getVentesCategorie(int max, int categorieId)throws SQLException{
        List<Vente> ventes = new ArrayList<>();

        PreparedStatement ps = laConnexion.prepareStatement("select * from VENTE natural join OBJET natural join UTILISATEUR natural join CATEGORIE  where idcat = ? order by idob limit ?");
		ps.setInt(1, categorieId);
        ps.setInt(2, max);

		ResultSet result = ps.executeQuery();
		while(result.next()){

            byte[] img = null;

            int idOb = result.getInt("idob");
            PreparedStatement ps2 = laConnexion.prepareStatement("select img from IMAGE_OBJET where idob = ?");
            ps2.setInt(1, idOb);
            ResultSet result2 = ps2.executeQuery();
            while(result2.next()){
                img = result2.getBytes("img");
            }

			int idVente = result.getInt("idve");
            double prixBase = result.getInt("prixbase");
            double prixMin = result.getInt("prixmin");
            Date debutVente = result.getDate("debutve");
            Date finVente = result.getDate("finve");

            Utilisateur utilisateur = new Utilisateur(result.getInt("idut"), result.getString("pseudout"), result.getString("emailut"), result.getString("mdput"), 0);
            Categorie categorie = new Categorie(result.getInt("idcat"), result.getString("nomcat"));

            Objet obj = new Objet(idOb, result.getString("nomob"), result.getString("descriptionob"), utilisateur, categorie, 0, img);

            Vente vente = new Vente(idVente, prixBase, prixMin, debutVente, finVente, obj);
            ventes.add(vente);
		}
		return ventes;
    }

    public List<Vente> getVentesNom(int max, String nom)throws SQLException{
        List<Vente> ventes = new ArrayList<>();

        PreparedStatement ps = laConnexion.prepareStatement("select * from VENTE natural join OBJET natural join UTILISATEUR natural join CATEGORIE  where nomob like ? order by idob limit ?");
        ps.setString(1, nom);
        ps.setInt(2, max);

        ResultSet result = ps.executeQuery();
        while(result.next()){

            byte[] img = null;

            int idOb = result.getInt("idob");
            PreparedStatement ps2 = laConnexion.prepareStatement("select img from IMAGE_OBJET where idob = ?");
            ps2.setInt(1, idOb);
            ResultSet result2 = ps2.executeQuery();
            while(result2.next()){
                img = result2.getBytes("img");
            }

            int idVente = result.getInt("idve");
            double prixBase = result.getInt("prixbase");
            double prixMin = result.getInt("prixmin");
            Date debutVente = result.getDate("debutve");
            Date finVente = result.getDate("finve");

            Utilisateur utilisateur = new Utilisateur(result.getInt("idut"), result.getString("pseudout"), result.getString("emailut"), result.getString("mdput"), 0);
            Categorie categorie = new Categorie(result.getInt("idcat"), result.getString("nomcat"));

            Objet obj = new Objet(idOb, result.getString("nomob"), result.getString("descriptionob"), utilisateur, categorie, 0, img);

            Vente vente = new Vente(idVente, prixBase, prixMin, debutVente, finVente, obj);
            ventes.add(vente);
        }
        return ventes;
    }

        public List<Objet> getVente(String pseudoUt) throws SQLException{
        List<Objet> res = new ArrayList<>();
        st = laConnexion.createStatement();
        ResultSet rs = st.executeQuery("select OBJET.* from UTILISATEUR natural join OBJET natural join VENTE natural join STATUT where pseudout='" + pseudoUt + "';");
        while(rs.next()){
			int idob = rs.getInt(1);
			String nomob = rs.getString(2);
			String descriptionob = rs.getString(3);
            Objet objet = new Objet(idob, nomob, descriptionob, null, null, 0, null);
            res.add(objet);
        }
		rs.close();
		return res;
    }

    public List<Double> getPrix(String pseudoUt) throws SQLException{
        List<Double> res = new ArrayList<>();
        st = laConnexion.createStatement();
        ResultSet rs = st.executeQuery("select prixbase from UTILISATEUR natural join OBJET natural join VENTE natural join STATUT where pseudout='" + pseudoUt + "';");
        while(rs.next()){
			Double prix = rs.getDouble(1);
            res.add(prix);
		}
		rs.close();
		return res;
    }
}