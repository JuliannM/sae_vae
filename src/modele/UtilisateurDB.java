package modele;

import java.sql.*;


public class UtilisateurDB{

	ConnexionMySQL laConnexion;
	Statement st;

	public UtilisateurDB(ConnexionMySQL laConnexion){
		this.laConnexion=laConnexion;
	}
	
	public int maxNumJoueur() throws SQLException{
		st = laConnexion.createStatement();
                ResultSet resultat = st.executeQuery("Select IFNULL(Max(idUt), 0) leMax from UTILISATEUR");
                resultat.next();
                int res = resultat.getInt(1);
				resultat.close();
        return res;
    }
	
	public void ajouterUtilisateur(Utilisateur e) throws  SQLException{
		PreparedStatement ps = laConnexion.prepareStatement(
				"INSERT INTO UTILISATEUR (idUt,pseudoUt,emailUT,mdpUt,activeUt,idRole) VALUES (?,?,?,?,?,?)");
		int nouvNum = maxNumJoueur()+1;
        ps.setInt(1, nouvNum);		
		ps.setString(2, e.getPseudonyme());
		ps.setString(3, e.getEmail());
		ps.setString(4, e.getMotDePasse());
		ps.setString(5, e.getActiveUt());
		ps.setInt(6, e.getRole());
		ps.executeUpdate();
		ps.close();
	};

	public void supprimerUtilisateur(String pseudo) throws SQLException{
		PreparedStatement ps = laConnexion.prepareStatement(
				"DELETE FROM UTILISATEUR WHERE pseudoUt = ?");
		ps.setString(1, pseudo);
		ps.executeUpdate();
		ps.close();
	};
	
	public void autoriserUtilisateur(String pseudo) throws SQLException{
		PreparedStatement ps = laConnexion.prepareStatement(
				"UPDATE UTILISATEUR SET activeUt = 'O' WHERE pseudoUt = ?");
		ps.setString(1, pseudo);
		ps.executeUpdate();
		ps.close();
	};
	
	public void revoquerUtilisateur(String pseudo) throws SQLException{
		PreparedStatement ps = laConnexion.prepareStatement(
				"UPDATE UTILISATEUR SET activeUt = 'N' WHERE pseudoUt = ?");
		ps.setString(1, pseudo);
		ps.executeUpdate();
		ps.close();
	};

	public boolean verifierEtConnecterUtilisateur(String pseudo, String motDePasse) throws SQLException {
		PreparedStatement ps = laConnexion.prepareStatement(
			"SELECT COUNT(*) FROM UTILISATEUR WHERE pseudoUt = ? AND mdpUt = ? AND activeUt = 'O'");
		ps.setString(1, pseudo);
		ps.setString(2, motDePasse);
	
		ResultSet resultat = ps.executeQuery();
		resultat.next();
		int nombreUtilisateurs = resultat.getInt(1);
		resultat.close();
		ps.close();
		System.out.println("Nombre d'utilisateurs trouvés: " + nombreUtilisateurs);
		if (nombreUtilisateurs > 0) {
			System.out.println("Utilisateur trouvé");
			return true;
		} else {
			return false;
		}
	}

	public boolean estAdmin(String pseudo)throws SQLException{
		PreparedStatement ps = laConnexion.prepareStatement("select idrole from UTILISATEUR where pseudout = ?");
		ps.setString(1, pseudo);
		ResultSet result = ps.executeQuery();
		while(result.next()){
			if(result.getInt("idrole") == 1){
				return true;
			}
		}
		return false;
	}

	public boolean pseudoExiste(String pseudo)throws SQLException{
		PreparedStatement ps = laConnexion.prepareStatement("select count(pseudout) nb from UTILISATEUR where pseudout = ?");
		ps.setString(1, pseudo);

		ResultSet result = ps.executeQuery();
		while(result.next()){
			if(result.getInt("nb") == 0){
				return false;
			}
		}
		return true;
	}

	public boolean emailExiste(String email)throws SQLException{
		PreparedStatement ps = laConnexion.prepareStatement("select count(emailut) nb from UTILISATEUR where emailut = ?");
		ps.setString(1, email);

		ResultSet result = ps.executeQuery();
		while(result.next()){
			if(result.getInt("nb") == 0){
				return false;
			}
		}
		return true;
	}
	public String getEmailPseudo(String pseudo) throws SQLException{
		PreparedStatement ps = laConnexion.prepareStatement("select emailut from UTILISATEUR where pseudout = ?");
		ps.setString(1, pseudo);

		ResultSet result = ps.executeQuery();
		while(result.next()){
			return result.getString("emailut");
		}
		return null;
	}

	
}

