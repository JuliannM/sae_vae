package modele;

import java.sql.*;

public class ConnexionMySQL {
	
	private Connection mysql=null;
	private boolean connecte=false;
	
	public ConnexionMySQL() throws ClassNotFoundException{
		this.mysql = null;
		this.connecte = false;
		Class.forName("org.mariadb.jdbc.Driver");
	}

	public void connecter() throws SQLException {
		// si tout c'est bien passé la connexion n'est plus nulle
		this.connecte=this.mysql!=null;
		this.mysql = null;
		this.connecte = false;
		this.mysql = DriverManager.getConnection("jdbc:mysql://servinfo-mariadb:3306/DBdepont","depont", "depont");
		this.connecte = true;
		System.out.println("Connexion réussie");
	}
	public void close() throws SQLException {
		// fermer la connexion
		this.mysql.close();
		this.connecte=false;
	}

    public boolean isConnecte(){ 
		return this.connecte;
	}

	public Statement createStatement() throws SQLException {
		return this.mysql.createStatement();
	}

	public PreparedStatement prepareStatement(String requete) throws SQLException{
		return this.mysql.prepareStatement(requete);
	}
	public Connection getMysql() {
		return mysql;
	}
}
